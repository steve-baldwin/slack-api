require "spec"
require "../src/slack-api"

module Spec::Methods
  def slack_it(description = "assert", file = __FILE__, line = __LINE__, end_line = __END_LINE__, &block)
    return unless Spec.matches?(description, file, line, end_line)

    Spec.formatters.each(&.before_example(description))

    start = Time.monotonic
    begin
      Spec.run_before_each_hooks
      block.call
      Spec::RootContext.report(:success, description, file, line, Time.monotonic - start)
    rescue ex : Slack::Request::MissingScope | Slack::Request::PaidTeamsOnly
      Spec::RootContext.report(:success, "[skipped] #{description}", file, line, Time.monotonic - start)
    rescue ex : Spec::AssertionFailed
      Spec::RootContext.report(:fail, description, file, line, Time.monotonic - start, ex)
      Spec.abort! if Spec.fail_fast?
    rescue ex
      Spec::RootContext.report(:error, description, file, line, Time.monotonic - start, ex)
      Spec.abort! if Spec.fail_fast?
    ensure
      Spec.run_after_each_hooks
    end
  end
end

private class LogFormatter < Spec::Formatter
  def initialize(@io)
  end

  def finish
    puts @io.to_s
  end
end

module SlackAPI
  @@params = {
    channel_history_id:     ENV["SLACK_ChannelHistoryId"],
    channel_info_id:        ENV["SLACK_ChannelInfoId"],
    channel_invite_user_id: ENV["SLACK_ChannelInviteUserId"],
    group_chat_users:       ENV["SLACK_GroupChatUsers"],
    channel_join_name:      ENV["SLACK_ChannelJoinName"],
    channel_join_id:        ENV["SLACK_ChannelJoinId"],
    chat_ephemeral_user_id: ENV["SLACK_ChatEphemeralUserId"],
    group_history_id:       ENV["SLACK_GroupHistoryId"],
    group_open_id:          ENV["SLACK_GroupOpenId"],
    im_user_id:             ENV["SLACK_IMUserId"],
    lookup_user_email:      ENV["SLACK_LookupUserEmail"],
  }

  Slack::Request.check_scope = true

  if ENV["SLACK_SpecLog"] == "Y"
    log_to = String::Builder.new("")
    fmt = LogFormatter.new(log_to)
    Spec.add_formatter(fmt)
    Slack::Request.set_default_logger(log_to)
    Slack::Request.log_level = Log::Severity::INFO
  end
end
