module SlackAPI
  @@channels = [] of Slack::Response::Conversation

  it "Can make an api.test call" do
    req = Slack::Request::APITest.new
    resp = req.submit
  end

  it "Can retrieve a list of all 'channel-like' things" do
    req = Slack::Request::ConversationsList.new(types: "public_channel,private_channel,im,mpim")
    if resp = req.submit
      if channels = resp.channels
        channels.each do |c|
          @@channels << c
        end
      end
    end
  end
end
