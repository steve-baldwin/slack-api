# slack-api Unit tests

Because these tests make calls to the Slack Web API, they need an OAuth access token for them to work. If you want to run these tests, you need to set up the token from your own Slack app in environment variable `SLACK_OAUTH_TOKEN`.

## Other environment variables
You will also need to set up a number of other `SLACK_xxx` environment variables to point to various objects in your Slack workspace that the tests need to run.  These environment variables are:

```bash
export SLACK_ChannelHistoryId=Cxxxxxxxx
export SLACK_ChannelInfoId=Cxxxxxxxx
export SLACK_ChannelInviteUserId=Uxxxxxxxx
export SLACK_GroupChatUsers="Uxxxxxxxx,Uxxxxxxxx"
export SLACK_ChannelJoinName=xxxxxxxx
export SLACK_ChannelJoinId=Cxxxxxxxx
export SLACK_ChatEphemeralUserId=Uxxxxxxxx
export SLACK_GroupHistoryId=Gxxxxxxxx
export SLACK_GroupOpenId=Gxxxxxxxx
export SLACK_IMUserId=Uxxxxxxxx
export SLACK_LookupUserEmail=abc@def.com
export SLACK_SpecLog=N
```

The environment variables are used for the following API calls:

| Variable | API Call(s) |
| -------- | --------- |
| `SLACK_ChannelHistoryId` | `channels.history`<br>`channels.mark`<br>`channels.replies`<br>`conversations.history`<br>`conversations.members`<br>`conversations.replies`<br>`pins.add`<br>`pins.list`<br>`pins.remove`<br>`usergroups.update` |
| `SLACK_ChannelInfoId` | `conversations.info`<br>`usergroups.update`<br>`channels.info` |
| `SLACK_ChannelInviteUserId` | `channels.invite`<br>`channels.kick`<br>`conversations.invite`<br>`conversations.kick`<br>`groups.invite`<br>`groups.kick`<br>`users.info`<br>`users.getPresence`<br>`users.profile.get` |
| `SLACK_GroupChatUsers` | `conversations.open`<br>`mpim.open`<br>`usergroups.users.update` |
| `SLACK_ChannelJoinName` | `channels.join` |
| `SLACK_ChannelJoinId` | `conversations.join`<br>`conversations.leave` |
| `SLACK_ChatEphemeralUserId` | `chat.postEphemeral`<br>`conversations.open` |
| `SLACK_GroupHistoryId` | `groups.history`<br>`groups.info`<br>`groups.mark`<br>`groups.replies` |
| `SLACK_GroupOpenId` | `groups.open`<br>`groups.leave` |
| `SLACK_IMUserId` | `im.open` |
| `SLACK_LookupUserEmail` | `users.lookupByEmail` |

## Test repeatability
The `groups.open` api call requires the API Slack user (i.e the user that installed the app) to have access to a private channel. The `groups.leave` API call will then attempt to leave that channel. In order for that call to succeed there must be at least one other user that is a member of that private channel. Once the `groups.leave` API call has completed, the API Slack user will no longer have access to that private channel, so if you immediately re-run the test suite, the `groups.open` and `groups.leave` tests will fail. In order for a re-run to suceed you will need to re-invite the API Slack user to that private channel from one of the existing members.

## Test cleanup
The test suite will create a number of public and private channels during a run (assuming appropriate OAuth scopes have been granted). There are currently no Slack API calls to delete public or private channels so you will need to clean them up yourself if this bothers you. If your Slack workspace is set up to prevent deleting channels you'll need to Archive them instead.

## Skipping tests
At this time the crystal unit test framework does not support skipping tests at run-time. You can describe them as `pending`, but that is something you set at compile time.

For this test suite I have created tests to execute all the Slack API methods contained in this library. However the tests will not execute successfully if your Slack app has not been granted the appropriate OAuth scopes. In this case, rather than reporting a failed test, the test suite will trap the Slack OAuth scope error and return success for the test.

In addition, there are some Slack API calls that are supported by this library that are only available to paid Slack subscribers. If you are running the test suite using an OAuth token that is **not** from a paid Slack account, the run-time error is trapped and those tests marked as successful.

## Logging
During development of this API I implemented an optional logging mechanism. By default when you run this test suite no logging other than the usual `crystal spec` output will occur. You can however set environment variable `SLACK_SpecLog` to `'Y'` and you will see the individual Slack API calls submitted. The logging output is 'deferred' (i.e not printed until all tests are complete) so it doesn't interfere with the usual spec output. It will look something like this:
```
Steve:slack-api stbaldwin$ crystal spec
...........................................................................................
I|11:11:29.706|00.012200|00.012200|Submitting api.test
I|11:11:30.066|00.371405|00.359205|Submitting conversations.list
I|11:11:30.410|00.716170|00.344765|Submitting channels.create
I|11:11:31.313|01.618470|00.902300|Submitting channels.history
I|11:11:31.646|01.952140|00.333670|Submitting channels.info
I|11:11:31.965|02.271053|00.318913|Submitting channels.invite
I|11:11:32.772|03.078273|00.807220|Submitting channels.join
:
```