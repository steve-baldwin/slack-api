module SlackAPI
  slack_it "Can pin a message" do
    req = Slack::Request::ConversationsHistory.new(channel: @@params[:channel_history_id])
    if resp = req.submit
      #
      # Find the first unpinned message
      #
      if messages = resp.messages
        messages.each do |msg|
          unless msg.pinned_to
            set_first_ts(msg.ts)
            break
          end
        end
        req2 = Slack::Request::PinsAdd.new(channel: @@params[:channel_history_id], timestamp: @@first_ts)
        resp2 = req2.submit
      end
    end
  end

  slack_it "Can list pinned items" do
    req = Slack::Request::PinsList.new(channel: @@params[:channel_history_id])
    resp = req.submit
  end

  slack_it "Can remove a message pin" do
    req = Slack::Request::PinsRemove.new(channel: @@params[:channel_history_id], timestamp: @@first_ts)
    resp = req.submit
  end
end
