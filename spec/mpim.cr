module SlackAPI
  slack_it "Can open an MPIM" do
    req = Slack::Request::MPIMOpen.new(users: @@params[:group_chat_users])
    if resp = req.submit
      set_group_channel(resp.group.id)
    end
  end

  slack_it "can retrieve history for an MPIM" do
    req = Slack::Request::MPIMHistory.new(channel: @@group_channel)
    if resp = req.submit
      if messages = resp.messages
        messages.reverse_each do |msg|
          if ts = msg.thread_ts
            set_first_ts(ts)
          end
        end
      end
    end
  end

  slack_it "returns deprecated method for groups.list call" do
    expect_raises(Slack::Request::Deprecated) do
      req = Slack::Request::MPIMList.new
      resp = req.submit
    end
  end

  slack_it "Can mark an MPIM" do
    req = Slack::Request::MPIMMark.new(channel: @@group_channel, ts: @@first_ts)
    resp = req.submit
  end

  slack_it "Can retrieve replies to an MPIM message" do
    req = Slack::Request::MPIMReplies.new(channel: @@group_channel, thread_ts: @@first_ts)
    resp = req.submit
  end

  slack_it "Can close an MPIM" do
    req = Slack::Request::MPIMClose.new(channel: @@group_channel)
    resp = req.submit
  end
end
