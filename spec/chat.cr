require "./spec_helper"

module SlackAPI
  slack_it "Can post a simple chat" do
    req = Slack::Request::ChatPostMessage.new(channel: @@public_channel, text: "Here is a simple message")
    resp = req.submit
  end

  slack_it "Can post a chat with a text attachment" do
    attach = Slack::Request::ParamsAttachment.new(fallback: "This is the fallback", callback_id: "doofus")
    attach.text = "Here is an attachment"
    attach.color = "good"
    attach.author_name = "Horrendo Revolver"
    req = Slack::Request::ChatPostMessage.new(channel: @@public_channel, text: "Here is a simple message", attachments: [attach])
    resp = req.submit
  end

  slack_it "Can post a chat with multiple attachments" do
    attach1 = Slack::Request::ParamsAttachment.new(fallback: "This is the fallback for 1", callback_id: "doofus1")
    attach1.text = "Here is an attachment"
    attach1.color = "good"
    attach1.author_name = "Horrendo Revolver"
    attach2 = Slack::Request::ParamsAttachment.new(fallback: "This is the fallback for 2", callback_id: "doofus2")
    attach2.text = "Here is another attachment"
    attach2.color = "warning"
    attach2.author_name = "Horrendo Revolver's Mum"
    req = Slack::Request::ChatPostMessage.new(
      channel: @@public_channel,
      text: "Here is a simple message",
      attachments: [attach1, attach2],
      )
    resp = req.submit
  end

  slack_it "Can post a message with an action button" do
    action = Slack::Request::ParamsAction.new("b1", "Click Me", "button")
    action.style = "danger"
    attach = Slack::Request::ParamsAttachment.new(fallback: "This is the fallback", callback_id: "doofus")
    attach.text = "Please click a button"
    attach.actions = [action]
    attach.author_name = "Doofus J. Dockweed"
    req = Slack::Request::ChatPostMessage.new(
      channel: @@public_channel,
      text: "Here's a button for you",
      attachments: [attach],
      )
    resp = req.submit
  end

  slack_it "Can post a message with two attachments and two action buttons" do
    attach1 = Slack::Request::ParamsAttachment.new(fallback: "This is the fallback for 1", callback_id: "doofus1")
    attach1.text = "Here is an attachment"
    attach1.color = "good"
    attach1.author_name = "Horrendo Revolver"
    action1 = Slack::Request::ParamsAction.new("b1", "Please Click Me", "button")
    action1.style = "primary"
    action2 = Slack::Request::ParamsAction.new("b2", "Don't Click Me", "button")
    action2.style = "danger"
    attach2 = Slack::Request::ParamsAttachment.new(fallback: "This is the fallback", callback_id: "doofus")
    attach2.text = "Please click a button"
    attach2.actions = [action1, action2]
    attach2.color = "#330066"
    req = Slack::Request::ChatPostMessage.new(
      channel: @@public_channel,
      text: "So many choices",
      attachments: [attach1, attach2],
      )
    resp = req.submit
  end

  slack_it "Can delete a chat" do
    req = Slack::Request::ChatPostMessage.new(channel: @@public_channel, text: "This message should vanish (unless delete is turned off at the workspace level)")
    if resp = req.submit
      ts = resp.ts
      req2 = Slack::Request::ChatDelete.new(channel: @@public_channel, ts: ts)
      resp2 = req2.submit
    end
  end

  slack_it "Can update a chat" do
    req = Slack::Request::ChatPostMessage.new(channel: @@public_channel, text: "This is the old message text")
    if resp = req.submit
      ts = resp.ts
      req2 = Slack::Request::ChatUpdate.new(channel: @@public_channel, ts: ts, text: "This is the new message text")
      resp2 = req2.submit
    end
  end

  slack_it "Can get the permalink of a chat" do
    req = Slack::Request::ChatPostMessage.new(channel: @@public_channel, text: "This is the old message text")
    if resp = req.submit
      ts = resp.ts
      req2 = Slack::Request::ChatGetPermalink.new(channel: @@public_channel, message_ts: ts)
      resp2 = req2.submit
    end
  end

  slack_it "Can post an ephemeral chat" do
    req = Slack::Request::ChatPostEphemeral.new(
      channel: @@public_channel,
      user: @@params[:chat_ephemeral_user_id],
      text: "Here is an ephemeral message",
      )
    resp = req.submit
  end

  slack_it "Can post a me-message" do
    req = Slack::Request::ChatMeMessage.new(channel: @@public_channel, text: "Here is a me-message")
    resp = req.submit
  end
end
