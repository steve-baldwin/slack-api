require "./spec_helper"
# require "./api"
# require "./channels"
# require "./chat"
# require "./conversations"
# require "./groups"
# require "./im"
# require "./mpim"
# require "./pins"
# require "./user-groups"
require "./users"

module SlackAPI
  extend self

  @@public_channel_name : String = ""
  @@private_channel_name : String = ""
  @@user_group_name : String = ""
  @@public_channel : String = ""
  @@private_channel : String = ""
  @@joined_channel : String = ""
  @@dm_channel : String = ""
  @@group_channel : String = ""
  @@user_group : String = ""
  @@first_ts : String = ""

  def self.set_public_channel_name
    @@public_channel_name = "aa-pub-" + Time.now.to_s("%s%L")
  end

  def self.set_private_channel_name
    @@private_channel_name = "aa-pri-" + Time.now.to_s("%s%L")
  end

  def self.set_user_group_name
    @@user_group_name = "usrgrp-" + Time.now.to_s("%s%L")
  end

  def self.set_public_channel(val)
    @@public_channel = val
  end

  def self.set_private_channel(val)
    @@private_channel = val
  end

  def self.set_group_channel(val)
    @@group_channel = val
  end

  def self.set_dm_channel(val)
    @@dm_channel = val
  end

  def self.set_joined_channel(val)
    @@joined_channel = val
  end

  def self.set_user_group(val)
    @@user_group = val
  end

  def self.set_first_ts(val)
    @@first_ts = val
  end
end
