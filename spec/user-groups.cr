module SlackAPI
  slack_it "Can create a User Group" do
    set_user_group_name()
    req = Slack::Request::UserGroupsCreate.new(name: @@user_group_name)
    if resp = req.submit
      if ug = resp.usergroup
        set_user_group(ug.id)
      end
    end
  end

  slack_it "Can list User Groups" do
    req = Slack::Request::UserGroupsList.new
    resp = req.submit
  end

  slack_it "Can update a User Group" do
    if @@user_group.size > 0
      req = Slack::Request::UserGroupsUpdate.new(
        usergroup: @@user_group,
        channels: "#{@@params[:channel_history_id]},#{@@params[:channel_info_id]}",
        description: "Some funky user group",
        include_count: true
      )
      resp = req.submit
    end
  end

  slack_it "Can disable a User Group" do
    if @@user_group.size > 0
      req = Slack::Request::UserGroupsDisable.new(usergroup: @@user_group)
      resp = req.submit
    end
  end

  slack_it "Can enable a User Group" do
    if @@user_group.size > 0
      req = Slack::Request::UserGroupsEnable.new(usergroup: @@user_group)
      resp = req.submit
    end
  end

  slack_it "Can update the users for a User Group" do
    if @@user_group.size > 0
      req = Slack::Request::UserGroupsUsersUpdate.new(
        usergroup: @@user_group,
        users: @@params[:group_chat_users],
      )
      resp = req.submit
    end
  end

  slack_it "Can get a list of users for a User Group" do
    if @@user_group.size > 0
      req = Slack::Request::UserGroupsUsersList.new(
        usergroup: @@user_group,
        include_disabled: true,
      )
      resp = req.submit
    end
  end
end
