module SlackAPI
  slack_it "can create a channel" do
    set_public_channel_name()
    req = Slack::Request::ChannelsCreate.new(@@public_channel_name)
    if resp = req.submit
      set_public_channel(resp.channel.id)
    end
  end

  slack_it "can retrieve history for a channel" do
    req = Slack::Request::ChannelsHistory.new(channel: @@params[:channel_history_id])
    if resp = req.submit
      set_first_ts(resp.messages[0].ts)
    end
  end

  slack_it "can retrieve info for a channel" do
    req = Slack::Request::ChannelsInfo.new(channel: @@params[:channel_info_id])
    resp = req.submit
  end

  slack_it "Can invite a user to a channel" do
    req = Slack::Request::ChannelsInvite.new(channel: @@public_channel, user: @@params[:channel_invite_user_id])
    resp = req.submit
  end

  slack_it "Can join a channel" do
    req = Slack::Request::ChannelsJoin.new(name: @@params[:channel_join_name])
    if resp = req.submit
      set_joined_channel(resp.channel.id)
    end
  end

  slack_it "Can kick a user from a channel" do
    req = Slack::Request::ChannelsKick.new(channel: @@public_channel, user: @@params[:channel_invite_user_id])
    resp = req.submit
  end

  slack_it "Can leave a channel" do
    req = Slack::Request::ChannelsLeave.new(channel: @@joined_channel)
    resp = req.submit
  end

  slack_it "returns deprecated method for channels.list call" do
    expect_raises(Slack::Request::Deprecated) do
      req = Slack::Request::ChannelsList.new
      resp = req.submit
    end
  end

  slack_it "Can mark a channel" do
    req = Slack::Request::ChannelsMark.new(channel: @@params[:channel_history_id], ts: @@first_ts)
    resp = req.submit
  end

  slack_it "Can rename a channel" do
    new_name = @@public_channel_name.sub("aa", "ab")
    req = Slack::Request::ChannelsRename.new(channel: @@public_channel, name: new_name)
    resp = req.submit
  end

  slack_it "Can retrieve replies to a channel message" do
    req = Slack::Request::ChannelsReplies.new(channel: @@params[:channel_history_id], thread_ts: @@first_ts)
    resp = req.submit
  end

  slack_it "Can set the purpose of a channel" do
    req = Slack::Request::ChannelsSetPurpose.new(channel: @@public_channel, purpose: "The quick brown dog")
    resp = req.submit
  end

  slack_it "Can set the topic of a channel" do
    req = Slack::Request::ChannelsSetTopic.new(channel: @@public_channel, topic: "Jumps over the lazy dog")
    resp = req.submit
  end

  slack_it "Can archive a channel" do
    req = Slack::Request::ChannelsArchive.new(channel: @@public_channel)
    resp = req.submit
  end

  slack_it "Can unarchive a channel" do
    req = Slack::Request::ChannelsUnarchive.new(channel: @@public_channel)
    resp = req.submit
  end
end
