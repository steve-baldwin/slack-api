module SlackAPI
  slack_it "can create a group" do
    set_private_channel_name()
    req = Slack::Request::GroupsCreate.new(@@private_channel_name)
    if resp = req.submit
      set_private_channel(resp.group.id)
    end
  end

  slack_it "can retrieve history for a group" do
    req = Slack::Request::GroupsHistory.new(channel: @@params[:group_history_id])
    if resp = req.submit
      if ts = resp.messages[0].thread_ts
        set_first_ts(ts)
      end
    end
  end

  slack_it "can retrieve info for a group" do
    req = Slack::Request::GroupsInfo.new(channel: @@params[:group_history_id])
    resp = req.submit
  end

  slack_it "Can invite a user to a group" do
    req = Slack::Request::GroupsInvite.new(channel: @@private_channel, user: @@params[:channel_invite_user_id])
    resp = req.submit
  end

  slack_it "Can open an existing group" do
    req = Slack::Request::GroupsOpen.new(channel: @@params[:group_open_id])
    resp = req.submit
  end

  slack_it "Can kick a user from a group" do
    req = Slack::Request::GroupsKick.new(channel: @@private_channel, user: @@params[:channel_invite_user_id])
    resp = req.submit
  end

  slack_it "Can leave a group" do
    req = Slack::Request::GroupsLeave.new(channel: @@params[:group_open_id])
    resp = req.submit
  end

  slack_it "returns deprecated method for groups.list call" do
    expect_raises(Slack::Request::Deprecated) do
      req = Slack::Request::GroupsList.new
      resp = req.submit
    end
  end

  slack_it "Can mark a group" do
    req = Slack::Request::GroupsMark.new(channel: @@params[:group_history_id], ts: @@first_ts)
    resp = req.submit
  end

  slack_it "Can rename a group" do
    new_name = @@private_channel_name.sub("aa", "ab")
    req = Slack::Request::GroupsRename.new(channel: @@private_channel, name: new_name)
    resp = req.submit
  end

  slack_it "Can retrieve replies to a group message" do
    req = Slack::Request::GroupsReplies.new(channel: @@params[:group_history_id], thread_ts: @@first_ts)
    resp = req.submit
  end

  slack_it "Can set the purpose of a group" do
    req = Slack::Request::GroupsSetPurpose.new(channel: @@private_channel, purpose: "The quick brown dog")
    resp = req.submit
  end

  slack_it "Can set the topic of a group" do
    req = Slack::Request::GroupsSetTopic.new(channel: @@private_channel, topic: "Jumps over the lazy dog")
    resp = req.submit
  end

  slack_it "Can archive a group" do
    req = Slack::Request::GroupsArchive.new(channel: @@private_channel)
    resp = req.submit
  end

  slack_it "Can unarchive a group" do
    req = Slack::Request::GroupsUnarchive.new(channel: @@private_channel)
    resp = req.submit
  end

  slack_it "Can create the child for a group" do
    req = Slack::Request::GroupsCreateChild.new(channel: @@private_channel)
    resp = req.submit
  end
end
