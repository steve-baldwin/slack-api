module SlackAPI

  slack_it "Can open an IM" do
    req = Slack::Request::IMOpen.new(user: @@params[:im_user_id])
    if resp = req.submit
      set_dm_channel(resp.channel.id)
    end
  end

  slack_it "can retrieve history for an IM" do
    req = Slack::Request::IMHistory.new(channel: @@dm_channel)
    if resp = req.submit
      if ts = resp.messages[0].thread_ts
        set_first_ts(ts)
      end
    end
  end

  slack_it "returns deprecated method for groups.list call" do
    expect_raises(Slack::Request::Deprecated) do
      req = Slack::Request::IMList.new
      resp = req.submit
    end
  end

  slack_it "Can mark an IM" do
    req = Slack::Request::IMMark.new(channel: @@dm_channel, ts: @@first_ts)
    resp = req.submit
  end


  slack_it "Can retrieve replies to an IM message" do
    req = Slack::Request::IMReplies.new(channel: @@dm_channel, thread_ts: @@first_ts)
    resp = req.submit
  end

  slack_it "Can close an IM" do
    req = Slack::Request::IMClose.new(channel: @@dm_channel)
    resp = req.submit
  end
end
