module SlackAPI
  slack_it "can create a public channel" do
    set_public_channel_name()
    req = Slack::Request::ConversationsCreate.new(@@public_channel_name, is_private: false)
    if resp = req.submit
      if channel = resp.channel
        set_public_channel(channel.id)
      end
    end
  end

  slack_it "can create a private channel" do
    set_private_channel_name()
    req = Slack::Request::ConversationsCreate.new(@@private_channel_name, is_private: true)
    if resp = req.submit
      if channel = resp.channel
        set_private_channel(channel.id)
      end
    end
  end

  slack_it "can retrieve history for a channel" do
    req = Slack::Request::ConversationsHistory.new(channel: @@params[:channel_history_id])
    if resp = req.submit
      set_first_ts(resp.messages[0].ts)
    end
  end

  slack_it "can retrieve info for a channel" do
    req = Slack::Request::ConversationsInfo.new(channel: @@params[:channel_info_id])
    resp = req.submit
  end

  slack_it "Can invite a user to a channel" do
    req = Slack::Request::ConversationsInvite.new(channel: @@public_channel, users: @@params[:channel_invite_user_id])
    resp = req.submit
  end

  slack_it "Can kick a user from a channel" do
    req = Slack::Request::ConversationsKick.new(channel: @@public_channel, user: @@params[:channel_invite_user_id])
    resp = req.submit
  end

  slack_it "Can join a channel" do
    req = Slack::Request::ConversationsJoin.new(channel: @@params[:channel_join_id])
    resp = req.submit
  end

  slack_it "Can leave a channel" do
    req = Slack::Request::ConversationsLeave.new(channel: @@params[:channel_join_id])
    resp = req.submit
  end

  slack_it "Can get a list of channels" do
    req = Slack::Request::ConversationsList.new
    resp = req.submit
  end

  slack_it "Can retrieve the members of a channel" do
    req = Slack::Request::ConversationsMembers.new(channel: @@params[:channel_history_id])
    resp = req.submit
  end

  slack_it "Can open a direct message with a user" do
    req = Slack::Request::ConversationsOpen.new(users: @@params[:chat_ephemeral_user_id], return_im: true)
    if resp = req.submit
      if channel = resp.channel
        set_dm_channel(channel.id)
      end
    end
  end

  slack_it "Can close a direct message with a user" do
    req = Slack::Request::ConversationsClose.new(channel: @@dm_channel)
    resp = req.submit
  end

  slack_it "Can open (and close) a group chat channel" do
    req = Slack::Request::ConversationsOpen.new(users: @@params[:group_chat_users])
    if resp = req.submit
      if channel = resp.channel
        set_group_channel(channel.id)
      end
      req2 = Slack::Request::ConversationsClose.new(channel: @@group_channel)
      resp2 = req2.submit
    end
  end

  slack_it "Can reopen (and close) a group chat channel by id" do
    req = Slack::Request::ConversationsOpen.new(channel: @@group_channel)
    if resp = req.submit
      req2 = Slack::Request::ConversationsClose.new(channel: @@group_channel)
      resp2 = req2.submit
    end
  end

  slack_it "Can rename a channel" do
    new_name = @@public_channel_name.sub("aa", "ab")
    req = Slack::Request::ConversationsRename.new(channel: @@public_channel, name: new_name)
    resp = req.submit
  end

  slack_it "Can retrieve replies to a channel message" do
    req = Slack::Request::ConversationsReplies.new(channel: @@params[:channel_history_id], ts: @@first_ts)
    resp = req.submit
  end

  slack_it "Can set the purpose of a channel" do
    req = Slack::Request::ConversationsSetPurpose.new(channel: @@public_channel, purpose: "The quick brown dog")
    resp = req.submit
  end

  slack_it "Can set the topic of a channel" do
    req = Slack::Request::ConversationsSetTopic.new(channel: @@public_channel, topic: "Jumps over the lazy dog")
    resp = req.submit
  end

  slack_it "Can archive a channel" do
    req = Slack::Request::ConversationsArchive.new(channel: @@public_channel)
    resp = req.submit
  end

  slack_it "Can unarchive a channel" do
    req = Slack::Request::ConversationsUnarchive.new(channel: @@public_channel)
    resp = req.submit
  end
end
