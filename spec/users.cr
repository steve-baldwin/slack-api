module SlackAPI
  slack_it "Can list user conversations" do
    req = Slack::Request::UsersConversations.new
    resp = req.submit
  end

  slack_it "Can list users" do
    req = Slack::Request::UsersList.new
    resp = req.submit
  end

  slack_it "Can lookup a user by email address" do
    req = Slack::Request::UsersLookupByEmail.new(email: @@params[:lookup_user_email])
    resp = req.submit
  end

  slack_it "Can retrieve info on a user" do
    req = Slack::Request::UsersInfo.new(user: @@params[:channel_invite_user_id])
    resp = req.submit
  end

  slack_it "Can retrieve a users presence information" do
    req = Slack::Request::UsersGetPresence.new(user: @@params[:channel_invite_user_id])
    resp = req.submit
  end

  slack_it "Can retrieve a users profile" do
    req = Slack::Request::UsersProfileGet.new(user: @@params[:channel_invite_user_id])
    resp = req.submit
  end
end
