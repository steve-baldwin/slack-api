require "../request"

module Slack::Request
  class MPIMMarkParams < ParamsJSON
    property channel : String
    property ts : String

    def initialize(@channel, @ts)
    end
  end

  class MPIMMark < Base
    def initialize(channel : String, ts : String)
      super("mpim.mark", MPIMMarkParams.new(channel, ts), "mpim:write")
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
