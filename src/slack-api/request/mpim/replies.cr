require "../request"
require "../../response/mpim/replies"

module Slack::Request
  class MPIMRepliesParams < Params
    property channel : String
    property thread_ts : String

    def initialize(@channel, @thread_ts)
    end
  end

  class MPIMReplies < Base
    def initialize(channel : String, thread_ts : String)
      super("mpim.replies", MPIMRepliesParams.new(channel, thread_ts), "mpim:history")
    end

    def get_response_class
      Slack::Response::MPIMReplies
    end
  end
end
