require "../request"
require "../../response"

module Slack::Request
  class MPIMCloseParams < ParamsJSON
    property channel : String

    def initialize(@channel)
    end
  end

  class MPIMClose < Base
    def initialize(channel : String)
      super("mpim.close", MPIMCloseParams.new(channel), Set{"mpim:write"})
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
