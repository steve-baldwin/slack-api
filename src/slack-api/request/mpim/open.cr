require "../request"
require "../../response/mpim/open"

module Slack::Request
  class MPIMOpenParams < ParamsJSON
    property users : String

    def initialize(@users)
    end
  end

  class MPIMOpen < Base
    def initialize(users : String)
      super("mpim.open", MPIMOpenParams.new(users), "mpim:write")
    end

    def get_response_class
      Slack::Response::MPIMOpen
    end
  end
end
