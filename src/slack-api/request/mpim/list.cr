require "../request"
require "../../response"

module Slack::Request
  # :nodoc:
  class MPIMListParams < Params
    property cursor : String?
    property limit = 20

    def initialize(@cursor, @limit)
    end
  end

  # DEPRECATED: Use `ConversationsList`
  class MPIMList < Base
    def initialize(cursor : String? = nil, limit = 20)
      @deprecated = true
      raise Slack::Exception::Deprecated.new("ConversationsList")
      super("mpim.list", MPIMListParams.new(cursor, limit), "mpim:read")
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
