require "../request"
require "../common/pins"

module Slack::Request
  class PinsRemove < Base
    def initialize(params : ParamsPins)
      super("pins.remove", params, "pins:write")
    end

    def initialize(channel : String, file : String? = nil, file_comment : String? = nil, timestamp : String? = nil)
      initialize(ParamsPins.new(channel, file, file_comment, timestamp))
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
