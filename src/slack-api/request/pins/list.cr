require "../request"
require "../../response/pins/list"

module Slack::Request
  class PinsListParams < Params
    property channel : String

    def initialize(@channel)
    end
  end

  class PinsList < Base
    def initialize(channel : String)
      super("pins.list", PinsListParams.new(channel), "pins:read")
    end

    def get_response_class
      Slack::Response::PinsList
    end
  end
end
