require "../request"

module Slack::Request
  # :nodoc:
  class APITestParams < Params
    property foo : String = "doofus"
    property error : String?
  end

  # This class is used to test basic Slack API access. It is used internally
  # by this shard to return a list of OAuth scopes for a token.
  class APITest < Base
    # :nodoc:
    def initialize
      super("api.test", APITestParams.new)
    end

    # :nodoc:
    def get_response_class
      Slack::Response::Success
    end
  end
end
