require "./action"
require "./field"

module Slack::Request
  class ParamsAttachment
    include JSON::Serializable
    property actions : Array(ParamsAction)?
    property attachment_type : String?
    property author_name : String?
    property author_link : String?
    property author_icon : String?
    property callback_id : String
    property color : String?
    property fallback : String
    property fields : Array(ParamsField)?
    property footer : String?
    property footer_icon : String?
    property image_url : String?
    property pretext : String?
    property text : String?
    property thumb_url : String?
    property title : String?
    property title_link : String?
    property ts : Int32?

    def initialize(@fallback : String, @callback_id : String)
    end
end
end
