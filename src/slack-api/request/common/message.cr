require "./attachment"

module Slack::Request
  class ParamsMessage < ParamsJSON
    property channel : String
    property text : String?
    property as_user = false
    property attachments : Array(ParamsAttachment)?
    property icon_emoji : String?
    property icon_url : String?
    property link_names = true
    property parse = "none"
    property reply_broadcast : Bool?
    property thread_ts : String?
    property unfurl_links : Bool?
    property unfurl_media : Bool?

    def initialize(@channel, @text, @attachments)
      unless @text || @attachments
        raise "Must supply text or attachments for a message."
      end
    end
  end
end
