require "./attachment"

module Slack::Request
  class ParamsPins < ParamsJSON
    property channel : String
    property file : String?
    property file_comment : String?
    property timestamp : String?

    def initialize(@channel, @file, @file_comment, @timestamp)
      n_not_nil = 0
      n_not_nil += 1 if @file
      n_not_nil += 1 if @file_comment
      n_not_nil += 1 if @timestamp
      if n_not_nil == 0
        raise "You must provide either file, file_comment or timestamp"
      end
      if n_not_nil > 1
        raise "The file, file_comment and timestamp params are mutually exclusive"
      end
    end
  end
end
