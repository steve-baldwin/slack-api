module Slack::Request
  class ParamsConfirmation
    include JSON::Serializable
    property title : String?
    property text : String
    property ok_text : String?
    property dismiss_text : String?

    def initialize(@text : String)
    end
  end

  class ParamsOption
    include JSON::Serializable
    property description : String?
    property text : String
    property value : String

    def initialize(@text : String, @value, @description = nil)
    end
  end

  class ParamsOptionGroup
    include JSON::Serializable
    property options : Array(ParamsOption)
    property text : String

    def initialize(@text : String, @options)
    end
  end

  class ParamsAction
    include JSON::Serializable
    property confirm : ParamsConfirmation?
    property data_source : String?
    property min_query_length : Int32?
    property name : String
    property options : Array(ParamsOption)?
    property option_groups : Array(ParamsOptionGroup)?
    property selected_options : Array(ParamsOption)?
    property style : String?
    property text : String?
    property type : String
    property value : String?

    def initialize(@name, @text, @type)
    end
  end
end
