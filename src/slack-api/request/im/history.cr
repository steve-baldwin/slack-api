require "../request"
require "../../response/common/history"

module Slack::Request
  class IMHistoryParams < Params
    property channel : String
    property count = 1000
    property unreads = true
    property inclusive = true
    property oldest = "0"
    property latest : String?

    def initialize(@channel, @count, @unreads, @inclusive, @oldest, @latest)
    end
  end

  class IMHistory < Base
    def initialize(params : IMHistoryParams)
      super("im.history", params, "im:history")
    end

    def initialize(channel : String, count = 1000, unreads = true, inclusive = true, oldest = "0", latest : String? = nil)
      initialize(IMHistoryParams.new(channel, count, unreads, inclusive, oldest, latest))
    end

    def get_response_class
      Slack::Response::History
    end
  end
end
