require "../request"
require "../../response"

module Slack::Request
  class IMCloseParams < ParamsJSON
    property channel : String

    def initialize(@channel)
    end
  end

  class IMClose < Base
    def initialize(channel : String)
      super("im.close", IMCloseParams.new(channel), Set{"im:write"})
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
