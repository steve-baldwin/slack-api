require "../request"
require "../../response/im/open"

module Slack::Request
  class IMOpenParams < ParamsJSON
    property user : String
    property include_locale = true
    property return_im = true

    def initialize(@user, @include_locale, @return_im)
    end
  end

  class IMOpen < Base
    def initialize(user : String, include_locale = true, return_im = true)
      super("im.open", IMOpenParams.new(user, include_locale, return_im), "im:write")
    end

    def get_response_class
      Slack::Response::IMOpen
    end
  end
end
