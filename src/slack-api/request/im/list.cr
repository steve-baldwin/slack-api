require "../request"
require "../../response"

module Slack::Request
  # :nodoc:
  class IMListParams < Params
    property cursor : String?
    property limit = 20

    def initialize(@cursor, @limit)
    end
  end

  # DEPRECATED: Use `ConversationsList`
  class IMList < Base
    def initialize(cursor : String? = nil, limit = 20)
      @deprecated = true
      raise Slack::Exception::Deprecated.new("ConversationsList")
      super("im.list", IMListParams.new(cursor, limit), "im:read")
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
