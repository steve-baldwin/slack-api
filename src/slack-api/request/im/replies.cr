require "../request"
require "../../response/im/replies"

module Slack::Request
  class IMRepliesParams < Params
    property channel : String
    property thread_ts : String

    def initialize(@channel, @thread_ts)
    end
  end

  class IMReplies < Base
    def initialize(channel : String, thread_ts : String)
      super("im.replies", IMRepliesParams.new(channel, thread_ts), "im:history")
    end

    def get_response_class
      Slack::Response::IMReplies
    end
  end
end
