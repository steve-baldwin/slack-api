require "../request"

module Slack::Request
  class IMMarkParams < ParamsJSON
    property channel : String
    property ts : String

    def initialize(@channel, @ts)
    end
  end

  class IMMark < Base
    def initialize(channel : String, ts : String)
      super("im.mark", IMMarkParams.new(channel, ts), "im:write")
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
