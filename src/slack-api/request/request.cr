require "log"
require "json"
require "http/client"
require "../response"
require "../exception"

class HTTP::Client
  property using_bot = false
end

module Slack::Request
  # SLACK_OAUTH_TOKEN  = "Bearer " + (ENV["SLACK_OAUTH_TOKEN"]? || "x")
  # SLACK_BOT_TOKEN    = "Bearer " + (ENV["SLACK_BOT_TOKEN"]? || "x")
  SLACK_API_BASE_URL = "slack.com"
  extend self

  @@oauth_token : String = (ENV["SLACK_OAUTH_TOKEN"]? || "x")
  @@bot_token : String = (ENV["SLACK_BOT_TOKEN"]? || "x")

  def self.oauth_token=(val : String)
    @@oauth_token = val
  end

  def self.bot_token=(val : String)
    @@bot_token = val
  end

  def self.check_scope=(val : Bool)
    Base.check_scope = val
  end

  def self.set_default_logger(io : IO = STDOUT)
    Base.set_default_logger(io)
  end

  def self.logger=(logger : Log)
    Base.logger = logger
  end

  def self.log_level=(val)
    Base.log_level = val
  end

  def get_client(use_bot = false)
    client = HTTP::Client.new(SLACK_API_BASE_URL, 443, true)
    client.using_bot = use_bot
    client.before_request do |r|
      r.headers["Authorization"] = "Bearer " + (use_bot ? @@bot_token : @@oauth_token)
    end
    client
  end

  abstract class Params
    protected def content_type
      "application/x-www-form-urlencoded"
    end

    protected def serialize : String
      p = HTTP::Params.new
      {% for ivar in @type.instance_vars %}
      if {{ivar.name}}
      	p.add({{ivar.name.stringify}}, {{ivar.name}}.to_s)
    	end
      {% end %}
      p.to_s
    end
  end

  abstract class ParamsJSON < Params
    include JSON::Serializable

    protected def content_type
      "application/json; charset=utf-8"
    end

    protected def serialize : String
      self.to_json
    end
  end

  abstract class Base
    @client : HTTP::Client? = nil
    @@logger : Log?
    @@check_scope = false
    @@granted_scopes : Set(String)? = nil

    @path : String
    @params : Params
    @response : Slack::Response?
    @required_scope : Set(String)? = nil

    protected def self.set_default_logger(io : IO = STDOUT)
      backend = Log::IOBackend.new(io)
      t_start = Time.local
      t_prev = t_start
      backend.formatter = Log::Formatter.new do |entry, io|
        total_time = entry.timestamp - t_start
        delta_time = entry.timestamp - t_prev
        t_prev = entry.timestamp
        io << entry.severity.label[0]
        io << "|" << entry.source
        io << "|" << entry.timestamp.to_s("%H:%M:%S.%L")
        io.printf("|%09.6f|%09.6f", total_time.total_seconds, delta_time.total_seconds)
        io << "|" << entry.message
      end
      builder = Log::Builder.new
      level = Log::Severity.parse(ENV.fetch("CRYSTAL_LOG_LEVEL", "INFO"))
      builder.bind("*", level, backend)
      @@logger = builder.for("slack-api")
    end

    protected def self.logger=(logger : Log)
      unless @@logger
        @@logger = logger
      end
      @@logger
    end

    protected def self.logger
      @@logger
    end

    protected def self.log_level=(val)
      @@logger.try &.level = val
    end

    # protected def self.log(level, msg)
    #   if l = @@logger
    #     l.log(level, msg)
    #   end
    # end

    protected def self.check_scope=(val : Bool)
      @@check_scope = val
    end

    protected def self.check_scope
      @@check_scope
    end

    protected def self.granted_scopes=(val)
      @@granted_scopes = val
    end

    protected def self.granted_scopes
      @@granted_scopes
    end

    protected def logger=(logger : Log)
      Base.logger = logger
    end

    protected def log_debug(msg)
      Base.logger.try &.debug { msg }
    end

    protected def log_info(msg)
      Base.logger.try &.info { msg }
    end

    protected def log_warn(msg)
      Base.logger.try &.warn { msg }
    end

    protected def log_error(msg)
      Base.logger.try &.error { msg }
    end

    protected def log_fatal(msg)
      Base.logger.try &.fatal { msg }
    end

    protected def params
      @params
    end

    protected def initialize(@path, @params)
    end

    protected def initialize(@path, @params, required_scope : String)
      @required_scope = Set{required_scope}
    end

    protected def initialize(@path, @params, @required_scope : Set(String))
    end

    private def check_scope
      #
      # No required scope for this request
      #
      return unless reqd = @required_scope
      #
      # We don't yet have the granted scopes. We'll make a call to api.test
      # to get them.
      #
      unless Base.granted_scopes
        Slack::Request::APITest.new.submit
      end

      return unless granted = Base.granted_scopes
      #
      # Are any of the required scopes in my set of granted scopes?
      #
      reqd.each do |s|
        return if granted.includes?(s)
      end
      raise Slack::Exception::MissingScope.new
    end

    def submit(client : HTTP::Client? = nil)
      unless client
        unless @client
          @client = Slack::Request.get_client
        end
        check_scope if Base.check_scope
        client = @client
      end
      more = true
      headers = HTTP::Headers.new
      headers["Content-Type"] = @params.content_type
      while client && more
        log_info("Submitting #{@path}")
        params = @params.serialize
        log_debug("params: #{params}")
        resp = client.post("/api/" + @path, headers, params)
        log_debug("Got response: #{resp.status_code}")
        if resp.success?
          more = false
          #
          # Grab the oauth scopes from the response header
          #
          unless client.using_bot
            if scopes = resp.headers["X-OAuth-Scopes"]?
              Base.granted_scopes = Set.new(scopes.split(','))
            end
          end
          #
          # Check whether or not the request was successful
          #
          if resp.body.includes?(%("ok":true))
            log_debug("Body: #{resp.body.pretty_inspect}")
            return get_response_class.from_json(resp.body)
          else
            failure = Slack::Response::Failure.from_json(resp.body)
            case failure.error
            when "paid_teams_only"
              raise Slack::Exception::PaidTeamsOnly.new(@path)
            else
              raise failure.error
            end
          end
        else
          #
          # Handle throttling
          #
          if (resp.status_code == 429) && (wait_for = resp.headers["Retry-After"]?)
            log_warn("#{@path}: Got TOO_MANY_REQUESTS response. Waiting for #{wait_for} seconds before retry.")
            sleep wait_for.to_i.seconds
          else
            more = false
            raise "#{resp.status_code}: #{resp.body}"
          end
        end
      end
    end

    protected abstract def get_response_class
  end
end

require "./common/*"
