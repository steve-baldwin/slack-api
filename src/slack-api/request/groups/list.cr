require "../request"

module Slack::Request
  # :nodoc:
  class GroupsListParams < Params
    property cursor : String?
    property exclude_archived = true
    property exclude_members = true
    property limit = 20

    def initialize(@cursor, @exclude_archived, @exclude_members, @limit)
    end
  end

  # DEPRECATED: Use `ConversationsList`
  class GroupsList < Base
    def initialize(cursor : String? = nil, exclude_archived = true, exclude_members = true, limit = 20)
      @deprecated = true
      raise Slack::Exception::Deprecated.new("ConversationsList")
      super("groups.list", GroupsListParams.new(cursor, exclude_archived, exclude_members, limit), "groups:read")
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
