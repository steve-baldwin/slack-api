require "../request"

module Slack::Request
  class GroupsUnarchiveParams < ParamsJSON
    property channel : String

    def initialize(@channel)
    end
  end

  class GroupsUnarchive < Base
    def initialize(channel : String)
      super("groups.unarchive", GroupsUnarchiveParams.new(channel), "groups:write")
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
