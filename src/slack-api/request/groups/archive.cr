require "../request"

module Slack::Request
  class GroupsArchiveParams < ParamsJSON
    property channel : String

    def initialize(@channel)
    end
  end

  class GroupsArchive < Base
    def initialize(channel : String)
      super("groups.archive", GroupsArchiveParams.new(channel), "groups:write")
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
