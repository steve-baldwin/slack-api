require "../request"
require "../../response/common/history"

module Slack::Request
  class GroupsHistoryParams < Params
    property channel : String
    property count = 1000
    property inclusive = true
    property oldest = "0"
    property latest : String?
    property unreads = true

    def initialize(@channel, @count, @unreads, @inclusive, @oldest, @latest)
    end
  end

  class GroupsHistory < Base
    def initialize(params : GroupsHistoryParams)
      super("groups.history", params, "groups:history")
    end

    def initialize(channel : String, count = 1000, unreads = true, inclusive = true, oldest = "0", latest : String? = nil)
      initialize(GroupsHistoryParams.new(channel, count, unreads, inclusive, oldest, latest))
    end

    def get_response_class
      Slack::Response::History
    end
  end
end
