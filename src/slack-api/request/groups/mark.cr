require "../request"

module Slack::Request
  class GroupsMarkParams < ParamsJSON
    property channel : String
    property ts : String

    def initialize(@channel, @ts)
    end
  end

  class GroupsMark < Base
    def initialize(channel : String, ts : String)
      super("groups.mark", GroupsMarkParams.new(channel, ts), "groups:write")
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
