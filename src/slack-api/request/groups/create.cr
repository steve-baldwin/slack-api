require "../request"
require "../../response/groups/info"

module Slack::Request
  class GroupsCreateParams < ParamsJSON
    property name : String
    property validate = true

    def initialize(@name)
    end
  end

  class GroupsCreate < Base
    def initialize(name : String)
      super("groups.create", GroupsCreateParams.new(name), "groups:write")
    end

    def get_response_class
      Slack::Response::GroupsInfo
    end
  end
end
