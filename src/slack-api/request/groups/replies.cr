require "../request"
require "../../response/groups/replies"

module Slack::Request
  class GroupsRepliesParams < Params
    property channel : String
    property thread_ts : String

    def initialize(@channel, @thread_ts)
    end
  end

  class GroupsReplies < Base
    def initialize(channel : String, thread_ts : String)
      super("groups.replies", GroupsRepliesParams.new(channel, thread_ts), "groups:history")
    end

    def get_response_class
      Slack::Response::GroupsReplies
    end
  end
end
