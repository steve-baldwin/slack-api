require "../request"
require "../../response/groups/rename"

module Slack::Request
  class GroupsRenameParams < ParamsJSON
    property channel : String
    property name : String
    property validate = true

    def initialize(@channel, @name)
    end
  end

  class GroupsRename < Base
    def initialize(channel : String, name : String)
      super("groups.rename", GroupsRenameParams.new(channel, name), "groups:write")
    end

    def get_response_class
      Slack::Response::GroupsRename
    end
  end
end
