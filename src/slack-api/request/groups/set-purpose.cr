require "../request"
require "../../response/groups/set-purpose"

module Slack::Request
  class GroupsSetPurposeParams < ParamsJSON
    property channel : String
    property purpose : String

    def initialize(@channel, @purpose)
    end
  end

  class GroupsSetPurpose < Base
    def initialize(channel : String, purpose : String)
      super("groups.setPurpose", GroupsSetPurposeParams.new(channel, purpose), "groups:write")
    end

    def get_response_class
      Slack::Response::GroupsSetPurpose
    end
  end
end
