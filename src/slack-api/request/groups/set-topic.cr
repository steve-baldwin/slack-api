require "../request"
require "../../response/groups/set-topic"

module Slack::Request
  class GroupsSetTopicParams < ParamsJSON
    property channel : String
    property topic : String

    def initialize(@channel, @topic)
    end
  end

  class GroupsSetTopic < Base
    def initialize(channel : String, topic : String)
      super("groups.setTopic", GroupsSetTopicParams.new(channel, topic), "groups:write")
    end

    def get_response_class
      Slack::Response::GroupsSetTopic
    end
  end
end
