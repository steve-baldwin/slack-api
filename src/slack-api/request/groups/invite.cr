require "../request"
require "../../response/groups/info"

module Slack::Request
  class GroupsInviteParams < ParamsJSON
    property channel : String
    property user : String

    def initialize(@channel, @user)
    end
  end

  class GroupsInvite < Base
    def initialize(channel : String, user : String)
      super("groups.invite", GroupsInviteParams.new(channel, user), "groups:write")
    end

    def get_response_class
      Slack::Response::GroupsInfo
    end
  end
end
