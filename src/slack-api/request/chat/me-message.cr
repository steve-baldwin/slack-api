require "../request"
require "../../response/chat/me-message"

module Slack::Request
  class ChatMeMessageParams < Params
    property channel : String
    property text : String

    def initialize(@channel, @text)
    end
  end

  class ChatMeMessage < Base
    def initialize(channel : String, text : String)
      super("chat.meMessage", ChatMeMessageParams.new(channel, text), "chat:write:user")
    end

    def get_response_class
      Slack::Response::ChatMeMessage
    end
  end
end
