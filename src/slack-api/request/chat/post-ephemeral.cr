require "../request"
require "../../response/chat/post-ephemeral"

module Slack::Request
  class ChatPostEphemeralParam < ParamsMessage
    property thread_ts : String?
    property user : String

    def initialize(@channel, @user, @text, @attachments)
      super(@channel, @text, @attachments)
    end
  end

  class ChatPostEphemeral < Base
    def initialize(params : ChatPostEphemeralParam)
      super("chat.postEphemeral", params, Set{"chat:write:user", "chat:write:bot"})
    end

    def initialize(channel : String, user : String, text : String? = nil, attachments : Array(Attachment)? = nil)
      initialize(ChatPostEphemeralParam.new(channel, user, text, attachments))
    end

    def get_response_class
      Slack::Response::ChatPostEphemeral
    end
  end
end
