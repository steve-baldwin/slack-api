require "../request"
require "../../response/chat/get-permalink"

module Slack::Request
  class ChatGetPermalinkParams < Params
    property channel : String
    property message_ts : String

    def initialize(@channel, @message_ts)
    end
  end

  class ChatGetPermalink < Base
    def initialize(channel : String, message_ts : String)
      super("chat.getPermalink", ChatGetPermalinkParams.new(channel, message_ts))
    end

    def get_response_class
      Slack::Response::ChatGetPermalink
    end
  end
end
