require "../request"
require "../../response/chat/delete"

module Slack::Request
  class ChatDeleteParams < ParamsJSON
    property channel : String
    property ts : String
    property as_user = false

    def initialize(@channel, @ts, @as_user)
    end
  end

  class ChatDelete < Base
    def initialize(channel : String, ts : String, as_user = false)
      super("chat.delete", ChatDeleteParams.new(channel, ts, as_user), Set{"chat:write:bot", "chat:write:user"})
    end

    def get_response_class
      Slack::Response::ChatDelete
    end
  end
end
