require "../request"
require "../../response/channels/info"

module Slack::Request
  class ChannelsInfoParams < Params
    property channel : String
    property include_locale = true

    def initialize(@channel)
    end
  end

  class ChannelsInfo < Base
    def initialize(channel : String)
      super("channels.info", ChannelsInfoParams.new(channel), "channels:read")
    end

    def get_response_class
      Slack::Response::ChannelsInfo
    end
  end
end
