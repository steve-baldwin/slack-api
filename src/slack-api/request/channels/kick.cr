require "../request"

module Slack::Request
  class ChannelsKickParams < ParamsJSON
    property channel : String
    property user : String

    def initialize(@channel, @user)
    end
  end

  class ChannelsKick < Base
    def initialize(channel : String, user : String)
      super("channels.kick", ChannelsKickParams.new(channel, user), "channels:write")
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
