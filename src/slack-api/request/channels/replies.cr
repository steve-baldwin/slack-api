require "../request"
require "../../response/channels/replies"

module Slack::Request
  class ChannelsRepliesParams < Params
    property channel : String
    property thread_ts : String

    def initialize(@channel, @thread_ts)
    end
  end

  class ChannelsReplies < Base
    def initialize(channel : String, thread_ts : String)
      super("channels.replies", ChannelsRepliesParams.new(channel, thread_ts), "channels:history")
    end

    def get_response_class
      Slack::Response::ChannelsReplies
    end
  end
end
