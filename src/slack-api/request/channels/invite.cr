require "../request"
require "../../response/channels/info"

module Slack::Request
  class ChannelsInviteParams < ParamsJSON
    property channel : String
    property user : String

    def initialize(@channel, @user)
    end
  end

  class ChannelsInvite < Base
    def initialize(channel : String, user : String)
      super("channels.invite", ChannelsInviteParams.new(channel, user), "channels:write")
    end

    def get_response_class
      Slack::Response::ChannelsInfo
    end
  end
end
