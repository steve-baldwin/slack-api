require "../request"
require "../../response/channels/info"

module Slack::Request
  class ChannelsRenameParams < ParamsJSON
    property channel : String
    property name : String
    property validate = true

    def initialize(@channel, @name)
    end
  end

  class ChannelsRename < Base
    def initialize(channel : String, name : String)
      super("channels.rename", ChannelsRenameParams.new(channel, name), "channels:write")
    end

    def get_response_class
      Slack::Response::ChannelsInfo
    end
  end
end
