require "../request"

module Slack::Request
  # :nodoc:
  class ChannelsListParams < Params
    property cursor : String?
    property exclude_archived = true
    property exclude_members = true
    property limit = 20

    def initialize(@cursor, @exclude_archived, @exclude_members, @limit)
    end
  end

  # DEPRECATED: Use `ConversationsList`
  class ChannelsList < Base
    def initialize(cursor : String? = nil, exclude_archived = true, exclude_members = true, limit = 20)
      @deprecated = true
      raise Slack::Exception::Deprecated.new("ConversationsList")
      super("channels.list", ChannelsListParams.new(cursor, exclude_archived, exclude_members, limit), "channels:read")
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
