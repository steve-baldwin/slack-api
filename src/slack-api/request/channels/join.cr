require "../request"
require "../../response/channels/join"

module Slack::Request
  class ChannelsJoinParams < ParamsJSON
    property name : String
    property validate = true

    def initialize(@name)
    end
  end

  class ChannelsJoin < Base
    def initialize(name : String)
      super("channels.join", ChannelsJoinParams.new(name), "channels:write")
    end

    def get_response_class
      Slack::Response::ChannelsJoin
    end
  end
end
