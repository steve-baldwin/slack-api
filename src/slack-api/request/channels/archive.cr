require "../request"

module Slack::Request
  class ChannelsArchiveParams < ParamsJSON
    property channel : String

    def initialize(@channel)
    end
  end

  class ChannelsArchive < Base
    def initialize(channel : String)
      super("channels.archive", ChannelsArchiveParams.new(channel), "channels:write")
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
