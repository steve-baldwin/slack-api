require "../request"

module Slack::Request
  class ChannelsUnarchiveParams < ParamsJSON
    property channel : String

    def initialize(@channel)
    end
  end

  class ChannelsUnarchive < Base
    def initialize(channel : String)
      super("channels.unarchive", ChannelsUnarchiveParams.new(channel), "channels:write")
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
