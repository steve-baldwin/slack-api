require "../request"
require "../../response/common/history"

module Slack::Request
  class ChannelsHistoryParams < Params
    property channel : String
    property count = 1000
    property unreads = true
    property inclusive = true
    property oldest = "0"
    property latest : String?

    def initialize(@channel, @count, @unreads, @inclusive, @oldest, @latest)
    end
  end

  class ChannelsHistory < Base
    def initialize(params : ChannelsHistoryParams)
      super("channels.history", params, "channels:history")
    end

    def initialize(channel : String, count = 1000, unreads = true, inclusive = true, oldest = "0", latest : String? = nil)
      initialize(ChannelsHistoryParams.new(channel, count, unreads, inclusive, oldest, latest))
    end

    def get_response_class
      Slack::Response::History
    end
  end
end
