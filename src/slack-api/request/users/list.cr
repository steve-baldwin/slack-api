require "../request"
require "../../response/users/list"

module Slack::Request
  class UsersListParams < Params
    property cursor : String?
    property include_locale = true
    property limit = 20

    def initialize(@cursor, @include_locale, @limit)
    end
  end

  class UsersList < Base
    def initialize(cursor : String? = nil, include_locale = true, limit = 20)
      super("users.list", UsersListParams.new(cursor, include_locale, limit), "users:read")
    end

    def get_response_class
      Slack::Response::UsersList
    end
  end
end
