require "../request"
require "../../response/users/get-presence"

module Slack::Request
  class UsersGetPresenceParams < Params
    property user : String

    def initialize(@user)
    end
  end

  class UsersGetPresence < Base
    def initialize(user : String)
      super("users.getPresence", UsersGetPresenceParams.new(user), "users:read")
    end

    def get_response_class
      Slack::Response::UsersGetPresence
    end
  end
end
