require "../request"
require "../../response/users/info"

module Slack::Request
  class UsersInfoParams < Params
    property user : String

    def initialize(@user)
    end
  end

  class UsersInfo < Base
    @@cache : Hash(String, Slack::Response::User) = {} of String => Slack::Response::User

    def initialize(user : String)
      super("users.info", UsersInfoParams.new(user), "users:read")
    end

    def get_response_class
      Slack::Response::UsersInfo
    end

    def submit(client : HTTP::Client? = nil)
      params = @params.as(UsersInfoParams)
      if user = @@cache[params.user]?
        resp = Slack::Response::UsersInfo.new(ok: true, user: user)
      else
        if resp = super(client)
          @@cache[params.user] = resp.user
        end
      end
      resp
    end
  end
end
