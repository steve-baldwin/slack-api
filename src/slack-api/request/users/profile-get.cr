require "../request"
require "../../response/users/profile-get"

module Slack::Request
  class UsersProfileGetParams < Params
    property user : String

    def initialize(@user)
    end
  end

  class UsersProfileGet < Base
    def initialize(user : String)
      super("users.profile.get", UsersProfileGetParams.new(user), "users.profile:read")
    end

    def get_response_class
      Slack::Response::UsersProfileGet
    end
  end
end
