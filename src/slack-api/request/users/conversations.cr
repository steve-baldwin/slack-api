require "../request"
require "../../response/conversations/list"

module Slack::Request
  class UsersConversationsParams < Params
    property cursor : String?
    property exclude_archived = true
    property limit = 20
    property types : String?
    property user : String?

    def initialize(@cursor, @exclude_archived, @limit, @types, @user)
    end
  end

  class UsersConversations < Base
    def initialize(params : UsersConversationsParams)
      super("users.conversations", params, Set{"channels:read", "groups:read", "im:read", "mpim:read"})
    end

    def initialize(cursor : String? = nil, exclude_archived = true, limit = 20, types : String? = nil, user : String? = nil)
      initialize(UsersConversationsParams.new(cursor, exclude_archived, limit, types, user))
    end

    def get_response_class
      Slack::Response::ConversationsList
    end
  end
end
