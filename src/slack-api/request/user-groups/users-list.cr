require "../request"
require "../../response/user-groups/list"

module Slack::Request
  class UserGroupsUsersListParams < Params
    property usergroup : String
    property include_disabled = false

    def initialize(@usergroup, @include_disabled)
    end
  end

  class UserGroupsUsersList < Base
    def initialize(usergroup : String, include_disabled = false)
      super("usergroups.users.list", UserGroupsUsersListParams.new(usergroup, include_disabled), "usergroups:read")
    end

    def get_response_class
      Slack::Response::UserGroupsUsersList
    end
  end
end
