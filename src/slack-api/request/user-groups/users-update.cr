require "../request"
require "../../response/user-groups/group"

module Slack::Request
  class UserGroupsUsersUpdateParams < Params
    property usergroup : String
    property users : String
    property include_count = true

    def initialize(@usergroup, @users, @include_count = true)
    end
  end

  class UserGroupsUsersUpdate < Base
    def initialize(params : UserGroupsUsersUpdateParams)
      super("usergroups.users.list", params, "usergroups:write")
    end

    def initialize(usergroup : String, users : String, include_count = true)
      initialize(UserGroupsUsersUpdateParams.new(usergroup, users, include_count))
    end

    def get_response_class
      Slack::Response::UserGroupsGroup
    end
  end
end
