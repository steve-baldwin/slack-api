require "../request"
require "../../response/user-groups/group"

module Slack::Request
  class UserGroupsCreateParams < Params
    property name : String
    property channels : String?
    property description : String?
    property handle : String?
    property include_count = false

    def initialize(@name, @channels, @description, @handle, @include_count)
    end
  end

  class UserGroupsCreate < Base
    def initialize(params : UserGroupsCreateParams)
      super("usergroups.create", params, "usergroups:write")
    end

    def initialize(name : String, channels : String? = nil, description : String? = nil, handle : String? = nil, include_count = false)
      initialize(UserGroupsCreateParams.new(name, channels, description, handle, include_count))
    end

    def get_response_class
      Slack::Response::UserGroupsGroup
    end
  end
end
