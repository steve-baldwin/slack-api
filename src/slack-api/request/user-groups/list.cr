require "../request"
require "../../response/user-groups/list"

module Slack::Request
  class UserGroupsListParams < Params
    property include_count = true
    property include_disabled = false
    property include_users = true

    def initialize(@include_count, @include_disabled, @include_users)
    end
  end

  class UserGroupsList < Base
    def initialize(include_count = true, include_disabled = false, include_users = true)
      super("usergroups.list", UserGroupsListParams.new(include_count, include_disabled, include_users), "usergroups:read")
    end

    def get_response_class
      Slack::Response::UserGroupsList
    end
  end
end
