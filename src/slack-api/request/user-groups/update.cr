require "../request"
require "../../response/user-groups/group"

module Slack::Request
  class UserGroupsUpdateParams < Params
    property usergroup : String
    property name : String?
    property channels : String?
    property description : String?
    property handle : String?
    property include_count = false

    def initialize(@usergroup, @name, @channels, @description, @handle, @include_count)
    end
  end

  class UserGroupsUpdate < Base
    def initialize(params : UserGroupsUpdateParams)
      super("usergroups.update", params, "usergroups:write")
    end

    def initialize(
      usergroup : String,
      name : String? = nil,
      channels : String? = nil,
      description : String? = nil,
      handle : String? = nil,
      include_count = false
    )
      initialize(UserGroupsUpdateParams.new(usergroup, name, channels, description, handle, include_count))
    end

    def get_response_class
      Slack::Response::UserGroupsGroup
    end
  end
end
