require "../request"
require "../../response/user-groups/group"

module Slack::Request
  class UserGroupsDisableParams < Params
    property usergroup : String
    property include_count = true

    def initialize(@usergroup, @include_count)
    end
  end

  class UserGroupsDisable < Base
    def initialize(usergroup, include_count = true)
      super("usergroups.disable", UserGroupsDisableParams.new(usergroup, include_count), "usergroups:write")
    end

    def get_response_class
      Slack::Response::UserGroupsGroup
    end
  end
end
