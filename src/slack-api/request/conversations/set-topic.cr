require "../request"
require "../../response/conversations/info"

module Slack::Request
  class ConversationsSetTopicParams < ParamsJSON
    property channel : String
    property topic : String

    def initialize(@channel, @topic)
    end
  end

  class ConversationsSetTopic < Base
    def initialize(channel : String, topic : String)
      super("conversations.setTopic", ConversationsSetTopicParams.new(channel, topic), Set{"channels:write", "groups:write", "im:write", "mpim:write"})
    end

    def get_response_class
      Slack::Response::ConversationsInfo
    end
  end
end
