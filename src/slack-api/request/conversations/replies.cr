require "../request"
require "../../response/conversations/history"

module Slack::Request
  class ConversationsRepliesParams < Params
    property channel : String
    property ts : String
    property cursor : String?
    property inclusive = false
    property latest : String?
    property limit = 20
    property oldest : String?

    def initialize(@channel, @ts)
    end
  end

  class ConversationsReplies < Base
    def initialize(params : ConversationsRepliesParams)
      super("conversations.replies", params, Set{"channels:history", "groups:history", "im:history", "mpim:history"})
    end

    def initialize(channel : String, ts : String)
      initialize(ConversationsRepliesParams.new(channel, ts))
    end

    def get_response_class
      Slack::Response::ConversationsHistory
    end
  end
end
