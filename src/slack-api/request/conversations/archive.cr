require "../request"

module Slack::Request
  class ConversationsArchiveParams < ParamsJSON
    property channel : String

    def initialize(@channel)
    end
  end

  class ConversationsArchive < Base
    def initialize(channel : String)
      super("conversations.archive", ConversationsArchiveParams.new(channel), Set{"channels:write", "groups:write", "im:write", "mpim:write"})
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
