require "../request"
require "../../response/common/history"

module Slack::Request
  class ConversationsHistoryParams < Params
    property channel : String
    property cursor : String?
    property inclusive = true
    property latest : String?
    property limit = 1000
    property oldest = "0"

    def initialize(@channel, @cursor, @inclusive, @latest, @limit, @oldest)
    end

    def initialize(@channel)
    end
  end

  class ConversationsHistory < Base
    def initialize(params : ConversationsHistoryParams)
      super("conversations.history", params, Set{"channels:history", "groups:history", "im:history", "mpim:history"})
    end

    def initialize(channel : String)
      initialize(ConversationsHistoryParams.new(channel))
    end

    def get_response_class
      Slack::Response::History
    end
  end
end
