require "../request"
require "../../response/conversations/info"

module Slack::Request
  class ConversationsRenameParams < ParamsJSON
    property channel : String
    property name : String

    def initialize(@channel, @name)
    end
  end

  class ConversationsRename < Base
    def initialize(channel : String, name : String)
      super("conversations.rename", ConversationsRenameParams.new(channel, name), Set{"channels:write", "groups:write", "im:write", "mpim:write"})
    end

    def get_response_class
      Slack::Response::ConversationsInfo
    end
  end
end
