require "../request"

module Slack::Request
  class ConversationsLeaveParams < ParamsJSON
    property channel : String

    def initialize(@channel)
    end
  end

  class ConversationsLeave < Base
    def initialize(channel : String)
      super("conversations.leave", ConversationsLeaveParams.new(channel), Set{"channels:write", "groups:write", "im:write", "mpim:write"})
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
