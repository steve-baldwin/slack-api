require "../request"
require "../../response/conversations/info"

module Slack::Request
  class ConversationsInfoParams < Params
    property channel : String
    property include_locale = true

    def initialize(@channel, @include_locale)
    end
  end

  class ConversationsInfo < Base
    def initialize(channel : String, include_locale = true)
      super("conversations.info", ConversationsInfoParams.new(channel, include_locale), Set{"channels:read", "groups:read", "im:read", "mpim:read"})
    end

    def get_response_class
      Slack::Response::ConversationsInfo
    end
  end
end
