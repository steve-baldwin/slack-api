require "../request"
require "../../response/conversations/list"

module Slack::Request
  class ConversationsListParams < Params
    property cursor : String?
    property exclude_archived = true
    property limit = 20
    property types : String?

    def initialize(@cursor, @exclude_archived, @limit, @types)
    end
  end

  class ConversationsList < Base
    def initialize(cursor : String? = nil, exclude_archived = true, limit = 20, types : String? = nil)
      super("conversations.list", ConversationsListParams.new(cursor, exclude_archived, limit, types), Set{"channels:read", "groups:read", "im:read", "mpim:read"})
    end

    def get_response_class
      Slack::Response::ConversationsList
    end
  end
end
