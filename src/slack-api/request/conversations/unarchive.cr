require "../request"

module Slack::Request
  class ConversationsUnarchiveParams < ParamsJSON
    property channel : String

    def initialize(@channel)
    end
  end

  class ConversationsUnarchive < Base
    def initialize(channel : String)
      super("conversations.unarchive", ConversationsUnarchiveParams.new(channel), Set{"channels:write", "groups:write", "im:write", "mpim:write"})
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
