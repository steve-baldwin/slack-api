require "../request"
require "../../response/conversations/join"

module Slack::Request
  class ConversationsJoinParams < ParamsJSON
    property channel : String

    def initialize(@channel)
    end
  end

  class ConversationsJoin < Base
    def initialize(channel : String)
      super("conversations.join", ConversationsJoinParams.new(channel), Set{"channels:write"})
    end

    def get_response_class
      Slack::Response::ConversationsJoin
    end
  end
end
