require "../request"

module Slack::Request
  class ConversationsKickParams < ParamsJSON
    property channel : String
    property user : String

    def initialize(@channel, @user)
    end
  end

  class ConversationsKick < Base
    def initialize(channel : String, user : String)
      super("conversations.kick", ConversationsKickParams.new(channel, user), Set{"channels:write", "groups:write", "im:write", "mpim:write"})
    end

    def get_response_class
      Slack::Response::Success
    end
  end
end
