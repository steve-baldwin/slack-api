require "../request"
require "../../response/conversations/info"

module Slack::Request
  class ConversationsOpenParams < ParamsJSON
    property channel : String?
    property return_im = false
    property users : String?

    def initialize(@channel, @return_im, @users)
      unless @channel || @users
        raise "You must provide a value for either channel or users"
      end
      if @channel && @users
        raise "You must provide a value for either channel or users, not both"
      end
    end
  end

  class ConversationsOpen < Base
    def initialize(channel : String? = nil, return_im = false, users : String? = nil)
      super("conversations.open", ConversationsOpenParams.new(channel, return_im, users), Set{"channels:write", "groups:write", "im:write", "mpim:write"})
    end

    def get_response_class
      Slack::Response::ConversationsInfo
    end
  end
end
