require "../request"
require "../../response/conversations/info"

module Slack::Request
  class ConversationsSetPurposeParams < ParamsJSON
    property channel : String
    property purpose : String

    def initialize(@channel, @purpose)
    end
  end

  class ConversationsSetPurpose < Base
    def initialize(channel : String, purpose : String)
      super("conversations.setPurpose", ConversationsSetPurposeParams.new(channel, purpose), Set{"channels:write", "groups:write", "im:write", "mpim:write"})
    end

    def get_response_class
      Slack::Response::ConversationsInfo
    end
  end
end
