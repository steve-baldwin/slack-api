require "./base"

module Slack::Event
  class IMOpen < Base
    property event : ChannelNameEvent
  end
end
