require "./base"

module Slack::Event
  class StarRemoved < Base
    property event : StarEvent
  end
end
