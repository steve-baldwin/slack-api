require "./base"

module Slack::Event
  class FilePublic < Base
    property event : FileEvent
  end
end
