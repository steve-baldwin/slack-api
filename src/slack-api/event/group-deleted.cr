require "./base"

module Slack::Event
  class GroupDeleted < Base
    property event : ChannelNameEvent
  end
end
