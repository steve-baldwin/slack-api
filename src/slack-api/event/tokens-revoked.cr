require "./base"

module Slack::Event
  class TokensRevokedInfo
    include JSON::Serializable
    property oauth : Array(String)?
    property bot : Array(String)?
  end

  class TokensRevokedEvent < TypeBase
    property tokens : TokensRevokedInfo
  end

  class TokensRevoked < Base
    property event : TokensRevokedEvent
  end
end
