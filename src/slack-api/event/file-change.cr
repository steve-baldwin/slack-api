require "./base"

module Slack::Event
  class FileChange < Base
    property event : FileCommentInfoEvent
  end
end
