require "./base"

module Slack::Event
  class GroupRename < Base
    property event : ChannelInfoEvent
  end
end
