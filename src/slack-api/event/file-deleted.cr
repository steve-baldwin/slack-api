require "./base"

module Slack::Event
  class FileDeleted < Base
    property event : FileEvent
  end
end
