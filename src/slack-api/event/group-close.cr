require "./base"

module Slack::Event
  class GroupClose < Base
    property event : ChannelNameEvent
  end
end
