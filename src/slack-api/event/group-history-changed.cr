require "./base"

module Slack::Event
  class GroupHistoryChanged < Base
    property event : ChannelHistoryChangedEvent
  end
end
