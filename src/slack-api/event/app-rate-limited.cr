require "./base"

module Slack::Event
  class AppRateLimited < Base
    property minute_rate_limited : Int64
  end
end
