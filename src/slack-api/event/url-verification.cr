require "./base"

module Slack::Event
  class URLVerification < Base
    property challenge : String

    def process(context)
      @challenge
    end
  end
end
