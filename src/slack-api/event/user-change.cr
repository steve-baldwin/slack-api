require "./base"
require "../response/common/user"

module Slack::Event
  class UserChangeEvent < TypeBase
    property user : Slack::Response::User
  end

  class UserChange < Base
    property event : UserChangeEvent
  end
end
