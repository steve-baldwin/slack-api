require "./base"

module Slack::Event
  class FileCommentDeleted < Base
    property event : FileCommentEvent
  end
end
