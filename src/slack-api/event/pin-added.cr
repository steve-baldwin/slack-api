require "./base"

module Slack::Event
  class PinAdded < Base
    property event : PinEvent
  end
end
