require "./base"

module Slack::Event
  class EmailDomainChangedEvent < TypeBase
    property email_domain : String
  end

  class EmailDomainChanged < Base
    property event : EmailDomainChangedEvent
  end
end
