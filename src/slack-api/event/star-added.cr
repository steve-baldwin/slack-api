require "./base"

module Slack::Event
  class StarAdded < Base
    property event : StarEvent
  end
end
