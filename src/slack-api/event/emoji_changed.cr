require "./base"

module Slack::Event
  class EmojiChangedEvent < TypeBase
    property subtype : String?
    property name : String?
    property names : Array(String)?
    property value : String?
  end

  class EmojiChanged < Base
    property event : EmojiChangedEvent
  end
end
