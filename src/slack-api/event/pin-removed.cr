require "./base"

module Slack::Event
  class PinRemoved < Base
    property event : PinEvent
  end
end
