require "./base"

module Slack::Event
  class ChannelRename < Base
    property event : ChannelInfoEvent
  end
end
