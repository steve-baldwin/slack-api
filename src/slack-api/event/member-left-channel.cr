require "./base"

module Slack::Event
  class MemberLeftChannel < Base
    property event : MemberChannelEvent
  end
end
