require "./base"

module Slack::Event
  class ChannelLeft < Base
    property event : ChannelNameEvent
  end
end
