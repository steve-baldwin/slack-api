require "./base"

module Slack::Event
  class ResourcesAdded < Base
    property event : ResourcesEvent
  end
end
