require "./base"

module Slack::Event
  class FileCommentEdited < Base
    property event : FileCommentInfoEvent
  end
end
