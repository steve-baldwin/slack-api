require "./base"

module Slack::Event
  class ChannelDeleted < Base
    property event : ChannelNameEvent
  end
end
