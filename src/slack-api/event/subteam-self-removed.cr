require "./base"

module Slack::Event
  class SubteamSelfRemoved < Base
    property event : SubteamSelfEvent
  end
end
