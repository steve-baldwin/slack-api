require "./base"

module Slack::Event
  class GroupUnarchive < Base
    property event : ChannelNameEvent
  end
end
