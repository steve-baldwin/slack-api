require "./base"

module Slack::Event
  class IMCreated < Base
    property event : ChannelInfoEvent
  end
end
