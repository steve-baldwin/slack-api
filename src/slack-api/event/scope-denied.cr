require "./base"

module Slack::Event
  class ScopeDenied < Base
    property event : ScopeEvent
  end
end
