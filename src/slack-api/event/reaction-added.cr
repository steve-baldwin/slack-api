require "./base"

module Slack::Event
  class ReactionAdded < Base
    property event : ReactionEvent
  end
end
