require "./base"

module Slack::Event
  class ScopeGranted < Base
    property event : ScopeEvent
  end
end
