require "./base"

module Slack::Event
  class FileCreated < Base
    property event : FileEvent
  end
end
