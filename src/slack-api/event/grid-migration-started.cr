require "./base"

module Slack::Event
  class GridMigrationStarted < Base
    property event : EnterpriseInfoEvent
  end
end
