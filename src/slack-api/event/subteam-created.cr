require "./base"

module Slack::Event
  class SubteamCreated < Base
    property event : SubteamEvent
  end
end
