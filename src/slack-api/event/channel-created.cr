require "./base"

module Slack::Event
  class ChannelCreated < Base
    property event : ChannelInfoEvent
  end
end
