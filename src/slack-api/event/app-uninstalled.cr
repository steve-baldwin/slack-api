require "./base"

module Slack::Event
  class AppUninstalled < Base
    property event : TypeBase
  end
end
