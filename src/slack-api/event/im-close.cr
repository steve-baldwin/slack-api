require "./base"

module Slack::Event
  class IMClose < Base
    property event : ChannelNameEvent
  end
end
