require "./base"

module Slack::Event
  class IMHistoryChanged < Base
    property event : ChannelHistoryChangedEvent
  end
end
