require "./base"

module Slack::Event
  class GroupLeft < Base
    property event : ChannelNameEvent
  end
end
