require "./base"

module Slack::Event
  class DNDUpdatedUser < Base
    property event : DNDInfoEvent
  end
end
