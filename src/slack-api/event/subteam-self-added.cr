require "./base"

module Slack::Event
  class SubteamSelfAdded < Base
    property event : SubteamSelfEvent
  end
end
