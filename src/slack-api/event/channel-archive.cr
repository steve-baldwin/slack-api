require "./base"

module Slack::Event
  class ChannelArchive < Base
    property event : ChannelNameEvent
  end
end
