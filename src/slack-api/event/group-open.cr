require "./base"

module Slack::Event
  class GroupOpen < Base
    property event : ChannelNameEvent
  end
end
