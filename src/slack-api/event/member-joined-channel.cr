require "./base"

module Slack::Event
  class MemberJoinedChannel < Base
    property event : MemberChannelEvent
  end
end
