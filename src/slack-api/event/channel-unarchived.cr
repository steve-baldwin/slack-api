require "./base"

module Slack::Event
  class ChannelUnarchive < Base
    property event : ChannelNameEvent
  end
end
