require "./base"

module Slack::Event
  class TeamDomainChangeEvent < TypeBase
    property url : String
    property domain : String
  end

  class TeamDomainChange < Base
    property event : TeamDomainChangeEvent
  end
end
