require "./base"

module Slack::Event
  class AppMentionEvent < TypeBase
    property text : String
    property user : String
    property channel : String
  end

  class AppMention < Base
    property event : AppMentionEvent
  end
end
