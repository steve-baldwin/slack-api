require "./base"

module Slack::Event
  class FileShared < Base
    property event : FileEvent
  end
end
