require "./base"

module Slack::Event
  class FileUnshared < Base
    property event : FileEvent
  end
end
