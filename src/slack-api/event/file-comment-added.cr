require "./base"

module Slack::Event
  class FileCommentAdded < Base
    property event : FileCommentInfoEvent
  end
end
