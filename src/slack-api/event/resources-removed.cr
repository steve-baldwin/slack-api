require "./base"

module Slack::Event
  class ResourcesRemoved < Base
    property event : ResourcesEvent
  end
end
