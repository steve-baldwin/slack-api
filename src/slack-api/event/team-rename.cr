require "./base"

module Slack::Event
  class TeamRenameEvent < TypeBase
    property name : String
  end

  class TeamRename < Base
    property event : TeamRenameEvent
  end
end
