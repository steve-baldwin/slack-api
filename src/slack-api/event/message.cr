require "./base"

module Slack::Event
  class MessageEdited
    include JSON::Serializable
    property user : String
    property ts : String
  end

  class MessageReaction
    include JSON::Serializable
    property name : String
    property count : Int32
    property users : Array(String)
  end

  class MessageEvent < TypeBase
    property text : String
    property user : String?
    property subtype : String?
    property channel : String
    property channel_type : String?
    property edited : MessageEdited?
    property hidden : Bool?
    property deleted_ts : String?
    property is_starred : Bool?
    property pinned_to : Array(String)?
    property reactions : Array(MessageReaction)?
  end

  class Message < Base
    property event : MessageEvent
  end
end
