require "./base"

module Slack::Event
  class DNDUpdated < Base
    property event : DNDInfoEvent
  end
end
