require "./base"

module Slack::Event
  class SubteamMembersEvent < TypeBase
    property subteam_id : String
    property team_id : String
    property date_previous_update : Int64
    property date_update : Int64
    property added_users : Array(String)?
    property added_users_count : String?
    property removed_users : Array(String)?
    property removed_users_count : String?
  end

  class SubteamMembersChanged < Base
    property event : SubteamMembersEvent
  end
end
