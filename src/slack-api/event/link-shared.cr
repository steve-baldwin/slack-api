require "./base"

module Slack::Event
  class LinkInfo
    include JSON::Serializable
    property domain : String
    property url : String
  end

  class LinkSharedEvent < TypeBase
    property channel : String
    property user : String
    property message_ts : String?
    property thread_ts : String?
    property links : Array(LinkInfo)?
  end

  class LinkShared < Base
    property event : LinkSharedEvent
  end
end
