require "./base"
require "../response/common/user"

module Slack::Event
  class TeamJoinEvent < TypeBase
    property user : Slack::Response::User
  end

  class TeamJoin < Base
    property event : TeamJoinEvent
  end
end
