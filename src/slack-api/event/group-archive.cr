require "./base"

module Slack::Event
  class GroupArchive < Base
    property event : ChannelNameEvent
  end
end
