require "./base"

module Slack::Event
  class ChannelHistoryChanged < Base
    property event : ChannelHistoryChangedEvent
  end
end
