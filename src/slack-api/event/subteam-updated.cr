require "./base"

module Slack::Event
  class SubteamUpdated < Base
    property event : SubteamEvent
  end
end
