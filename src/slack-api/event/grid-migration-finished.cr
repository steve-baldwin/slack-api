require "./base"

module Slack::Event
  class GridMigrationFinished < Base
    property event : EnterpriseInfoEvent
  end
end
