require "json"
require "../response/common/user-group"

module Slack::Event
  extend self
  TYPE_MAP = {
    "app_mention"             => AppMention,
    "app_rate_limited"        => AppRateLimited,
    "app_uninstalled"         => AppUninstalled,
    "channel_archive"         => ChannelArchive,
    "channel_created"         => ChannelCreated,
    "channel_deleted"         => ChannelDeleted,
    "channel_history_changed" => ChannelHistoryChanged,
    "channel_left"            => ChannelLeft,
    "channel_rename"          => ChannelRename,
    "channel_unarchive"       => ChannelUnarchive,
    "dnd_updated"             => DNDUpdated,
    "dnd_updated_user"        => DNDUpdatedUser,
    "email_domain_changed"    => EmailDomainChanged,
    "emoji_changed"           => EmojiChanged,
    "file_change"             => FileChange,
    "file_comment_added"      => FileCommentAdded,
    "file_comment_edited"     => FileCommentEdited,
    "file_comment_deleted"    => FileCommentDeleted,
    "file_created"            => FileCreated,
    "file_deleted"            => FileDeleted,
    "file_public"             => FilePublic,
    "file_shared"             => FileShared,
    "file_unshared"           => FileUnshared,
    "grid_migration_finished" => GridMigrationFinished,
    "grid_migration_started"  => GridMigrationStarted,
    "group_archive"           => GroupArchive,
    "group_close"             => GroupClose,
    "group_deleted"           => GroupDeleted,
    "group_history_changed"   => GroupHistoryChanged,
    "group_left"              => GroupLeft,
    "group_open"              => GroupOpen,
    "group_rename"            => GroupRename,
    "group_unarchive"         => GroupUnarchive,
    "im_close"                => IMClose,
    "im_created"              => IMCreated,
    "im_history_changed"      => IMHistoryChanged,
    "im_open"                 => IMOpen,
    "link_shared"             => LinkShared,
    "member_joined_channel"   => MemberJoinedChannel,
    "member_left_channel"     => MemberLeftChannel,
    "message"                 => Message,
    "pin_added"               => PinAdded,
    "pin_removed"             => PinRemoved,
    "reaction_added"          => ReactionAdded,
    "reaction_removed"        => ReactionRemoved,
    "resources_added"         => ResourcesAdded,
    "resources_removed"       => ResourcesRemoved,
    "scope_denied"            => ScopeDenied,
    "scope_granted"           => ScopeGranted,
    "star_added"              => StarAdded,
    "star_removed"            => StarRemoved,
    "subteam_created"         => SubteamCreated,
    "subteam_members_changed" => SubteamMembersChanged,
    "subteam_self_added"      => SubteamSelfAdded,
    "subteam_self_removed"    => SubteamSelfRemoved,
    "subteam_updated"         => SubteamUpdated,
    "team_domain_change"      => TeamDomainChange,
    "team_join"               => TeamJoin,
    "team_rename"             => TeamRename,
    "tokens_revoked"          => TokensRevoked,
    "url_verification"        => URLVerification,
    "user_change"             => UserChange,
  }

  def from_body(body : String)
    base = Base.from_json(body)
    if c = TYPE_MAP[base.type]?
      return c.from_json(body)
    end
    base = BaseE.from_json(body)
    if event = base.event
      if c = TYPE_MAP[event.type]?
        return c.from_json(body)
      end
    end
    puts ">>> Unrecognised event:"
    puts JSON.parse(body).to_pretty_json
    return nil
  rescue e
    puts ">>> JSON error (%s) parsing:\n%s" % [e.message, body]
  end

  class TypeBase
    include JSON::Serializable
    property type : String
    property event_ts : String?
    property ts : String?
  end

  class Base
    include JSON::Serializable
    property token : String
    property type : String
    property team_id : String?
    property api_app_id : String?
    property authed_users : Array(String)?
    property authed_teams : Array(String)?
    property event_id : String?
    property event_time : Int64?

    def process(context : HTTP::Server::Context) : String?
      puts ">>> Default event processing for #{self.class}"
      nil
    end
  end

  class BaseE < Base
    property event : TypeBase
  end

  class ChannelNameEvent < TypeBase
    property channel : String
    property user : String?
  end

  class ChannelInfo
    include JSON::Serializable
    property id : String
    property name : String
    property created : Int64?
    property creator : String?
  end

  class ChannelInfoEvent < TypeBase
    property channel : ChannelInfo
    property user : String?
  end

  class ChannelHistoryChangedEvent < TypeBase
    property channel : String?
    property latest : String
  end

  class DNDInfo
    include JSON::Serializable
    property dnd_enabled : Bool
    property next_dnd_start_ts : Int64
    property next_dnd_end_ts : Int64
    property snooze_enabled : Bool?
    property snooze_endtime : Int64?
  end

  class DNDInfoEvent < TypeBase
    property user : String
    property dnd_status : DNDInfo
  end

  class FileCommentInfo
    include JSON::Serializable
    property id : String
    property created : Int64
    property timestamp : Int64
    property user : String
    property comment : String
    property channel : String
  end

  class FileInfo
    include JSON::Serializable
    property id : String
  end

  class FileEvent < TypeBase
    property file_id : String
    property file : FileInfo?
  end

  class FileCommentEvent < FileEvent
    property comment : String
  end

  class FileCommentInfoEvent < FileEvent
    property comment : FileCommentInfo
  end

  class EnterpriseInfoEvent < TypeBase
    property enterprise_id : String
  end

  class ItemInfo
    include JSON::Serializable
    property type : String
    property channel : String?
    property ts : String?
    property file : String?
    property file_comment : String?
  end

  class MemberChannelEvent < TypeBase
    property channel : String
    property channel_type : String
    property team : String
    property inviter : String?
    property user : String?
  end

  class PinEvent < TypeBase
    property item : ItemInfo
    property user : String?
    property channel_id : String
    property has_pins : Bool?
  end

  class ReactionEvent < TypeBase
    property item : ItemInfo
    property user : String?
    property reaction : String
    property item_user : String
  end

  class ResourceGrantInfo
    include JSON::Serializable
    property type : String
    property resource_id : String
  end

  class ResourceInfo
    include JSON::Serializable
    property type : String
    property grant : ResourceGrantInfo
  end

  class ResourceScopeInfo
    include JSON::Serializable
    property resource : ResourceInfo
    property scopes : Array(String)
  end

  class ResourcesEvent < TypeBase
    property resources : Array(ResourceScopeInfo)
  end

  class ScopeEvent < TypeBase
    property scopes : Array(String)
    property trigger_id : String
  end

  class StarEvent < TypeBase
    property item : ItemInfo
    property user : String?
  end

  class SubteamEvent < TypeBase
    property subteam : Slack::Response::UserGroup
  end

  class SubteamSelfEvent < TypeBase
    property subteam_id : String
  end
end

require "./*"
