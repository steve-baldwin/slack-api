require "./base"

module Slack::Event
  class ReactionRemoved < Base
    property event : ReactionEvent
  end
end
