module Slack::Exception
  #
  # This exception is raised when a request is submitted using OAuth credentials
  # of a 'free-tier' Slack account that is only available to paid Slack accounts.
  class PaidTeamsOnly < ::Exception
    # :nodoc:
    def initialize(call : String)
      super("Sorry - the Slack API call (#{call}) is only available to paid-up teams.")
    end
  end

  #
  # This exception is raised when a request object is created and the active OAuth token
  # has not been granted the required scope. Note that this check is made during `#initialize`
  # and is only made if `Slack::Request::check_scope` has been set to `true`.
  class MissingScope < ::Exception
    # :nodoc:
    def initialize
      super("No OAuth scope for request")
    end
  end

  # This exception is raised when a request object is created and the underlying
  # Slack API call has been marked as deprecated. The exception will indicate the
  # alternate call that should be used instead.
  class Deprecated < ::Exception
    # :nodoc:
    def initialize(instead : String)
      super("This method has been deprecated. Use #{instead} instead.")
    end
  end
end
