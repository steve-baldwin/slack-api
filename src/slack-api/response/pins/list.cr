require "../common/message"
require "../common/file"

module Slack::Response
  class PinnedItem
    include JSON::Serializable
    property channel : String?
    property created : Int32
    property created_by : String?
    property message : Message?
    property file : File?
    property comment : String?
    property type : String
  end

  class PinsList < Success
    property items : Array(PinnedItem)?
  end
end
