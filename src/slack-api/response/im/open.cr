require "../response"
require "../common/conversation"

module Slack::Response
  class IMOpen < Success
    property channel : Conversation
    property already_open : Bool?
    property no_op : Bool?
  end
end
