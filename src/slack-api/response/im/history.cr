require "../common/message"

module Slack::Response
  class IMHistory < Success
    property has_more : Bool?
    property messages : Array(Message)
    property latest : String?
    property unread_count_display : Int32?
  end
end
