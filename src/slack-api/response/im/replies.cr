require "../common/message"

module Slack::Response
  class IMReplies < Success
    property messages : Array(Message)
  end
end
