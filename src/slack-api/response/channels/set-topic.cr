module Slack::Response
  class ChannelsSetTopic < Success
    property topic : String?
  end
end
