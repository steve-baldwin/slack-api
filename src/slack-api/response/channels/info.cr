require "../common/conversation"

module Slack::Response
  class ChannelsInfo < Success
    property channel : Conversation
  end
end
