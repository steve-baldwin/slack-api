require "../common/conversation"

module Slack::Response
  class GroupsRename < Success
    property channel : Conversation
  end
end
