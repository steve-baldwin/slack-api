require "../common/message"

module Slack::Response
  class GroupsReplies < Success
    property messages : Array(Message)
  end
end
