module Slack::Response
  class GroupsSetPurpose < Success
    property purpose : String?
  end
end
