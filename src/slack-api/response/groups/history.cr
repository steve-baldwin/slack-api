require "../common/message"

module Slack::Response
  class GroupsHistory < Success
    property latest : String?
    property messages : Array(Message)
    property has_more : Bool?
    property unread_count_display : Int32?
  end
end
