require "./*"

module Slack::Response
  class MessageIcon
    include JSON::Serializable
    property emoji : String
    property image_64 : String?
  end

  class MessageReply
    include JSON::Serializable
    property ts : String
    property user : String
  end

  class PinnedInfo
    include JSON::Serializable
    property channel : String?
    property pinned_by : String?
    property ts : Int32?
  end

  class Message
    include JSON::Serializable
    property attachments : Array(Attachment)?
    property bot_id : String?
    property comment : Comment?
    property display_as_bot : Bool?
    property file : File?
    property icons : MessageIcon?
    property inviter : String?
    property is_intro : Bool?
    property last_read : String?
    property mrkdown : Bool?
    property name : String?
    property old_name : String?
    property parent_user_id : String?
    property permalink : String?
    property pinned_info : PinnedInfo?
    property pinned_to : Array(String)?
    property purpose : String?
    property reactions : Array(Reaction)?
    property replies : Array(MessageReply)?
    property reply_count : Int32?
    property source_team : String?
    property subscribed : Bool?
    property subtype : String?
    property team : String?
    property text : String?
    property thread_ts : String?
    property topic : String?
    property ts : String
    property type : String
    property unread_count : Int32?
    property upload : Bool?
    property user : String?
    property user_profile : UserProfile?
    property user_team : String?
  end
end
