module Slack::Response
  class UserGroupPrefs
    include JSON::Serializable
    property channels : Array(String)?
    property groups : Array(String)?
  end

  class UserGroup
    include JSON::Serializable
    property id : String
    property team_id : String
    property is_usergroup : Bool
    property name : String
    property description : String?
    property handle : String?
    property is_external : Bool
    property date_create : Int32
    property date_update : Int32?
    property date_delete : Int32?
    property auto_type : String?
    property created_by : String
    property updated_by : String?
    property deleted_by : Bool?
    property prefs : UserGroupPrefs?
    property users : Array(String)?
    property user_count : String?
  end
end
