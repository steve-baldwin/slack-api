module Slack::Response
  class Share
    include JSON::Serializable
    property accepted_user : String
    property is_active : Bool
    property team : Team
    property user : String
  end
end
