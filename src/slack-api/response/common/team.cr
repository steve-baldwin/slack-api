module Slack::Response
  class TeamIcon
    include JSON::Serializable
    property image_102 : String?
    property image_132 : String?
    property image_230 : String?
    property image_34 : String?
    property image_44 : String?
    property image_68 : String?
    property image_88 : String?
    property image_default : Bool?
  end

  class TeamStatusPreset
    include JSON::Serializable
    property items : String
  end

  class TeamStatusPresets
    include JSON::Serializable
    property items : Array(TeamStatusPreset)
  end

  class TeamCreationRequest
    include JSON::Serializable
    property is_enabled : Bool
  end

  class TeamPrefs
    include JSON::Serializable
    property all_users_can_purchase : Bool?
    property allow_calls : Bool?
    property allow_calls_interactive_screen_sharing : Bool?
    property allow_message_deletion : Bool?
    property allow_retention_override : Bool?
    property allow_shared_channel_perms_override : Bool?
    property app_whitelist_enabled : Bool?
    property auth_mode : String?
    property calling_app_name : String?
    property can_receive_shared_channels_invites : Bool?
    property compliance_export_start : Int32?
    property custom_status_default_emoji : String?
    property custom_status_presets : TeamStatusPresets?
    property custom_tos : Bool?
    property default_channels : Array(String)
    property default_rxns : Array(String)?
    property disable_email_ingestion : Bool?
    property disable_file_deleting : Bool?
    property disable_file_editing : Bool?
    property disable_file_uploads : String?
    property disallow_public_file_urls : Bool?
    property discoverable : String?
    property display_email_addresses : Bool?
    property display_real_names : Bool?
    property dm_retention_duration : Int32?
    property dm_retention_type : Int32?
    property dnd_enabled : Bool?
    property dnd_end_hour : String?
    property dnd_start_hour : String?
    property enable_shared_channels : Int32?
    property enterprise_default_channels : Array(String)?
    property enterprise_mandatory_channels : Array(String)?
    property enterprise_mdm_date_enabled : Int32?
    property enterprise_mdm_level : Int32?
    property enterprise_team_creation_request : TeamCreationRequest
    property file_limit_whitelisted : Bool?
    property file_retention_duration : Int32?
    property file_retention_type : Int32?
    property gdrive_enabled_team : Bool?
    property group_retention_duration : Int32?
    property group_retention_type : Int32?
    property hide_referers : Bool?
    property invites_limit : Bool?
    property invites_only_admins : Bool?
    property locale : String?
    property loud_channel_mentions_limit : Int32?
    property msg_edit_window_mins : Int32?
    property retention_duration : Int32?
    property retention_type : Int32?
    property show_join_leave : Bool?
    property uses_customized_custom_status_presets : Bool?
    property warn_before_at_channel : String?
    property who_can_archive_channels : String?
    property who_can_at_channel : String?
    property who_can_at_everyone : String?
    property who_can_change_team_profile : String?
    property who_can_create_channels : String?
    property who_can_create_delete_user_groups : String?
    property who_can_create_groups : String?
    property who_can_create_shared_channels : String?
    property who_can_edit_user_groups : String?
    property who_can_kick_channels : String?
    property who_can_kick_groups : String?
    property who_can_manage_guests : StringItemsType?
    property who_can_manage_integrations : StringItemsType?
    property who_can_manage_shared_channels : StringItemsType?
    property who_can_post_general : String?
    property who_can_post_in_shared_channels : StringItemsType?
    property who_has_team_visibility : String?
  end

  class Team
    include JSON::Serializable
    property avatar_base_url : String?
    property domain : String
    property email_domain : String
    property enterprise_id : String?
    property enterprise_name : String?
    property has_compliance_export : Bool?
    property icon : TeamIcon
    property id : String
    property message_count : Int32?
    property msg_edit_window_mins : Int32?
    property name : String
    property over_integrations_limit : Bool?
    property over_storage_limit : Bool?
    property plan : String?
    property prefs : TeamPrefs?
  end

  class TeamProfileField
    include JSON::Serializable
    property field_name : String?
    property hint : String
    property id : String
    property is_hidden : Bool?
    property label : String
    property options : Array(String)
    property ordering : Int32
    property possible_values : Array(String)?
    property type : String
  end
end
