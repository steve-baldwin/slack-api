require "./user-profile"

module Slack::Response
  class User
    include JSON::Serializable
    property color : String
    property deleted : Bool
    property has_2fa : Bool?
    property id : String
    property is_admin : Bool
    property is_app_user : Bool
    property is_bot : Bool
    property is_owner : Bool
    property is_primary_owner : Bool
    property is_restricted : Bool
    property is_ultra_restricted : Bool
    property locale : String?
    property name : String
    property presence : String?
    property profile : UserProfile
    property real_name : String
    property team_id : String
    property tz : String?
    property tz_label : String?
    property tz_offset : Int32?
    property updated : Int32
  end
end
