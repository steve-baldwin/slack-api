module Slack::Response
  class ResponseMetadata
    include JSON::Serializable
    property next_cursor : String?
    property warnings : Array(String)?
  end
end
