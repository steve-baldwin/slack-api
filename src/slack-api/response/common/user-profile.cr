module Slack::Response
  class UserProfile
    include JSON::Serializable
    property always_active : Bool?
    property avatar_hash : String?
    property display_name : String
    property display_name_normalized : String?
    property email : String?
    property first_name : String?
    property guest_channels : String?
    property image_192 : String?
    property image_24 : String?
    property image_32 : String?
    property image_48 : String?
    property image_512 : String?
    property image_72 : String?
    property image_original : String?
    property last_name : String?
    property phone : String?
    property real_name : String
    property real_name_normalized : String?
    property skype : String?
    property status_emoji : String?
    property status_expiration : Int32?
    property status_text : String?
    property status_text_canonical : String?
    property team : String?
    property title : String?
  end

  class UserProfileShort
    include JSON::Serializable
    property avatar_hash : String
    property display_name : String
    property first_name : String
    property image_72 : String
    property is_restricted : Bool
    property is_ultra_restricted : Bool
    property name : String
    property real_name : String
    property team : String
  end

  class UserProfileShortest
    include JSON::Serializable
    property avatar_hash : String
    property display_name : String
    property first_name : String
    property image_72 : String
    property real_name : String
    property team : String
  end
end
