require "../response"
require "./*"

module Slack::Response
  class Conversation
    include JSON::Serializable
    property accepted_user : String?
    property created : Int32
    property creator : String?
    property display_counts : DisplayCounts?
    property has_pins : Bool?
    property id : String
    property is_archived : Bool?
    property is_channel : Bool?
    property is_ext_shared : Bool?
    property is_general : Bool?
    property is_group : Bool?
    property is_im : Bool?
    property is_member : Bool?
    property is_moved : Int32?
    property is_mpim : Bool?
    property is_open : Bool?
    property is_org_shared : Bool?
    property is_pending_ext_shared : Bool?
    property is_private : Bool?
    property is_read_only : Bool?
    property is_shared : Bool?
    property last_read : String?
    property latest : Message?
    property members : Array(String)?
    property name : String?
    property name_normalized : String?
    property num_members : Int32?
    property pending_shared : Array(String)?
    property pin_count : Int32?
    property previous_names : Array(String)?
    property priority : (Int32 | Float64)?
    property purpose : CLV?
    property shared_team_ids : Array(String)?
    property shares : Array(Share)?
    property timezone_count : Int32?
    property topic : CLV?
    property unlinked : Int32?
    property unread_count : Int32?
    property unread_count_display : Int32?
    property user : String?
  end
end
