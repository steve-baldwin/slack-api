module Slack::Response
  class Paging
    include JSON::Serializable
    property count : Int32
    property page : Int32
    property pages : Int32?
    property total : Int32
  end
end
