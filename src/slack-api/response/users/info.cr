require "../common/user"

module Slack::Response
  class UsersInfo < Success
    property user : User

    def initialize(@ok, @user)
    end
  end
end
