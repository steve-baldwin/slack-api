require "../common/user"
require "../common/response-metadata"

module Slack::Response
  class UsersList < Success
    property members : Array(User)?
    property cache_ts : Int32?
    property response_metadata : ResponseMetadata?
  end
end
