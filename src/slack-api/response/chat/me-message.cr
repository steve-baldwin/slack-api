module Slack::Response
  class ChatMeMessage < Success
    property channel : String
    property ts : String
  end
end
