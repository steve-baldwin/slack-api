module Slack::Response
  class ChatUpdate < Success
    property channel : String
    property ts : String
    property text : String?
  end
end
