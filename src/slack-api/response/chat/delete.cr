module Slack::Response
  class ChatDelete < Success
    property channel : String
    property ts : String
  end
end
