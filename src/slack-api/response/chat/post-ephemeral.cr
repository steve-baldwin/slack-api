module Slack::Response
  class ChatPostEphemeral < Success
    property message_ts : String
  end
end
