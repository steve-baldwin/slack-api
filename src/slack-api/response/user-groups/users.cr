require "../common/user-group"

module Slack::Response
  class UserGroupsUsersList < Success
    property users : Array(String)
  end
end
