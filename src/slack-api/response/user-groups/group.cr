require "../common/user-group"

module Slack::Response
  class UserGroupsGroup < Success
    property usergroup : UserGroup
  end
end
