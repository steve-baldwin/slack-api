require "../common/message"

module Slack::Response
  class MPIMReplies < Success
    property messages : Array(Message)
  end
end
