require "../common/conversation"

module Slack::Response
  class MPIMOpen < Success
    property group : Conversation
  end
end
