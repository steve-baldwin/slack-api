require "../common/conversation"
require "../common/response-metadata"

module Slack::Response
  class ConversationsList < Success
    property channels : Array(Conversation)?
    property response_metadata : ResponseMetadata?
  end
end
