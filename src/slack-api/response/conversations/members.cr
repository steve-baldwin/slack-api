require "../common/response-metadata"

module Slack::Response
  class ConversationsMembers < Success
    property members : Array(String)
    property response_metadata : ResponseMetadata?
  end
end
