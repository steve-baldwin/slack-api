require "../common/message"
require "../common/response-metadata"

module Slack::Response
  class ConversationsHistory < Success
    property has_more : Bool?
    property messages : Array(Message)
    property pin_count : Int32?
    property response_metadata : ResponseMetadata?
  end
end
