require "../common/conversation"
require "../common/response-metadata"

module Slack::Response
  class ConversationsJoin < Success
    property channel : Conversation
    property warning : String?
    property response_metadata : ResponseMetadata?
  end
end
