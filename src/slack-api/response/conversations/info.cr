require "../common/conversation"

module Slack::Response
  class ConversationsInfo < Success
    property channel : Conversation?
  end
end
