# slack-cr

A Crystal shard for making [Slack Web API](https://api.slack.com/methods) calls. Most (but not all) API calls have been implemented. If there are calls missing for what you want to do, either send a request to the contributors or (ideally) submit a pull request.

## Installation

Add this to your application's `shard.yml`:

```yaml
dependencies:
  slack-api:
    github: crystal-bits/slack-api
```

## Usage

In order to make any Slack API call you need to set up a Slack app, and create an OAuth user token and optionally a Bot user token. These tokens are described [here](https://api.slack.com/docs/token-types). The user token needs to be set in environment variable `SLACK_OAUTH_TOKEN`. If you are using the Bot user token (for example to send messages as the Bot user rather than the user that installed the app), you need to set it in environment variable `SLACK_BOT_TOKEN`.

From there it is pretty straight forward. A class has been defined for each of the supported Slack API calls. The instance variables of the class correspond to the parameters defined for the Slack API method being called. You simply instantiate the class and call the `#submit` method on that object.

For example:

```crystal
  require "slack-api"

  req = Slack::Request::APITest.new
  resp = req.submit
```

The response object will contain all the fields as defined in the Slack API docs.

For examples of specific calls, refer to spec tests.

## Checking OAuth scopes

As you will be aware, access to individual Slack API methods is controlled by OAuth scope. This shard can optionally check whether or not the current OAuth token has been granted the required OAuth scope prior to making the call. In order for this checking to occur simply do the following:

```crystal
  Slack::Request.check_scope = true
```

## Throttling (aka Rate Limits)

Slack imposes rate limits on most of the API requests. This shard handles any response from Slack as per [Slack docs](https://api.slack.com/docs/rate-limits#headers) and resubmits requests (after sleeping for the directed duration) automatically as necessary.

## Logging

The shard has been set up to log various events in case you're interested. I mainly used it during development but you may find it useful. There are two options for logging. Either you provide your own `Log` instance as follows:

```crystal
  Slack::Request.logger = my_logger
```

or have the shard create a 'default' logger for you:

```crystal
  Slack::Request.set_default_logger
```

The following messages will be logged. The examples are from the default logger:

### Submitting a Slack API http request

- Level = `Log::Severity::INFO`
- `I|14:33:03.369|00.019301|00.019301|Submitting api.test`

### Dump request parameters before `POST`

- Level = `Log::Severity::DEBUG`
- `D|14:33:17.714|00.333320|00.000037|params: {"exclude_archived":true,"limit":20}`

### Response from Slack API http request

- Level = `Log::Severity::DEBUG`
- `D|14:33:18.036|00.654655|00.321335|Got response: 200`

### Rate Limit response

- Level = `Log::Severity::WARN`
- `W|14:33:18.997|01.615490|00.309931|chat.postMessage : Got TOO_MANY_REQUESTS response. Waiting for 5 seconds before retry.`

## Class to API mapping

Here is a list of the supported Slack API method calls and the corresponding `slack-api` class.

### Channels Methods

- [channels.archive](https://api.slack.com/methods/channels.archive) : `Slack::Request::ChannelsArchive`
- [channels.create](https://api.slack.com/methods/channels.create) : `Slack::Request::ChannelsCreate`
- [channels.history](https://api.slack.com/methods/channels.history) : `Slack::Request::ChannelsHistory`
- [channels.info](https://api.slack.com/methods/channels.info) : `Slack::Request::ChannelsInfo`
- [channels.invite](https://api.slack.com/methods/channels.invite) : `Slack::Request::ChannelsInvite`
- [channels.join](https://api.slack.com/methods/channels.join) : `Slack::Request::ChannelsJoin`
- [channels.kick](https://api.slack.com/methods/channels.kick) : `Slack::Request::ChannelsKick`
- [channels.leave](https://api.slack.com/methods/channels.leave) : `Slack::Request::ChannelsLeave`
- [channels.list](https://api.slack.com/methods/channels.list) : `Slack::Request::ChannelsList`
- [channels.mark](https://api.slack.com/methods/channels.mark) : `Slack::Request::ChannelsMark`
- [channels.rename](https://api.slack.com/methods/channels.rename) : `Slack::Request::ChannelsRename`
- [channels.replies](https://api.slack.com/methods/channels.replies) : `Slack::Request::ChannelsReplies`
- [channels.setPurpose](https://api.slack.com/methods/channels.setPurpose) : `Slack::Request::ChannelsSetPurpose`
- [channels.setTopic](https://api.slack.com/methods/channels.setTopic) : `Slack::Request::ChannelsSetTopic`
- [channels.unarchive](https://api.slack.com/methods/channels.unarchive) : `Slack::Request::ChannelsUnarchive`

### Chat Methods

- [chat.delete](https://api.slack.com/methods/chat.delete) : `Slack::Request::ChatDelete`
- [chat.getPermalink](https://api.slack.com/methods/chat.getPermalink) : `Slack::Request::ChatGetPermalink`
- [chat.meMessage](https://api.slack.com/methods/chat.meMessage) : `Slack::Request::ChatMeMessage`
- [chat.postEphemeral](https://api.slack.com/methods/chat.postEphemeral) : `Slack::Request::ChatPostEphemeral`
- [chat.postMessage](https://api.slack.com/methods/chat.postMessage) : `Slack::Request::ChatPostMessage`
- [chat.update](https://api.slack.com/methods/chat.update) : `Slack::Request::ChatUpdate`

### Conversations Methods

- [conversations.archive](https://api.slack.com/methods/conversations.archive) : `Slack::Request::ConversationsArchive`
- [conversations.close](https://api.slack.com/methods/conversations.close) : `Slack::Request::ConversationsClose`
- [conversations.create](https://api.slack.com/methods/conversations.create) : `Slack::Request::ConversationsCreate`
- [conversations.history](https://api.slack.com/methods/conversations.history) : `Slack::Request::ConversationsHistory`
- [conversations.info](https://api.slack.com/methods/conversations.info) : `Slack::Request::ConversationsInfo`
- [conversations.invite](https://api.slack.com/methods/conversations.invite) : `Slack::Request::ConversationsInvite`
- [conversations.join](https://api.slack.com/methods/conversations.join) : `Slack::Request::ConversationsJoin`
- [conversations.kick](https://api.slack.com/methods/conversations.kick) : `Slack::Request::ConversationsKick`
- [conversations.leave](https://api.slack.com/methods/conversations.leave) : `Slack::Request::ConversationsLeave`
- [conversations.list](https://api.slack.com/methods/conversations.list) : `Slack::Request::ConversationsList`
- [conversations.members](https://api.slack.com/methods/conversations.members) : `Slack::Request::ConversationsMembers`
- [conversations.open](https://api.slack.com/methods/conversations.open) : `Slack::Request::ConversationsOpen`
- [conversations.rename](https://api.slack.com/methods/conversations.rename) : `Slack::Request::ConversationsRename`
- [conversations.replies](https://api.slack.com/methods/conversations.replies) : `Slack::Request::ConversationsReplies`
- [conversations.setPurpose](https://api.slack.com/methods/conversations.setPurpose) : `Slack::Request::ConversationsSetPurpose`
- [conversations.setTopic](https://api.slack.com/methods/conversations.setTopic) : `Slack::Request::ConversationsSetTopic`
- [conversations.unarchive](https://api.slack.com/methods/conversations.unarchive) : `Slack::Request::ConversationsUnarchive`

### Groups Methods

- [groups.archive](https://api.slack.com/methods/groups.archive) : `Slack::Request::GroupsArchive`
- [groups.createChild](https://api.slack.com/methods/groups.createChild) : `Slack::Request::GroupsCreateChild`
- [groups.create](https://api.slack.com/methods/groups.create) : `Slack::Request::GroupsCreate`
- [groups.history](https://api.slack.com/methods/groups.history) : `Slack::Request::GroupsHistory`
- [groups.info](https://api.slack.com/methods/groups.info) : `Slack::Request::GroupsInfo`
- [groups.invite](https://api.slack.com/methods/groups.invite) : `Slack::Request::GroupsInvite`
- [groups.kick](https://api.slack.com/methods/groups.kick) : `Slack::Request::GroupsKick`
- [groups.leave](https://api.slack.com/methods/groups.leave) : `Slack::Request::GroupsLeave`
- [groups.list](https://api.slack.com/methods/groups.list) : `Slack::Request::GroupsList`
- [groups.mark](https://api.slack.com/methods/groups.mark) : `Slack::Request::GroupsMark`
- [groups.open](https://api.slack.com/methods/groups.open) : `Slack::Request::GroupsOpen`
- [groups.rename](https://api.slack.com/methods/groups.rename) : `Slack::Request::GroupsRename`
- [groups.replies](https://api.slack.com/methods/groups.replies) : `Slack::Request::GroupsReplies`
- [groups.setPurpose](https://api.slack.com/methods/groups.setPurpose) : `Slack::Request::GroupsSetPurpose`
- [groups.setTopic](https://api.slack.com/methods/groups.setTopic) : `Slack::Request::GroupsSetTopic`
- [groups.unarchive](https://api.slack.com/methods/groups.unarchive) : `Slack::Request::GroupsUnarchive`

### IM Methods

- [im.close](https://api.slack.com/methods/im.close) : `Slack::Request::IMClose`
- [im.history](https://api.slack.com/methods/im.history) : `Slack::Request::IMHistory`
- [im.list](https://api.slack.com/methods/im.list) : `Slack::Request::IMList`
- [im.mark](https://api.slack.com/methods/im.mark) : `Slack::Request::IMMark`
- [im.open](https://api.slack.com/methods/im.open) : `Slack::Request::IMOpen`
- [im.replies](https://api.slack.com/methods/im.replies) : `Slack::Request::IMReplies`

### MPIM Methods

- [mpim.close](https://api.slack.com/methods/mpim.close) : `Slack::Request::MPIMClose`
- [mpim.history](https://api.slack.com/methods/mpim.history) : `Slack::Request::MPIMHistory`
- [mpim.list](https://api.slack.com/methods/mpim.list) : `Slack::Request::MPIMList`
- [mpim.mark](https://api.slack.com/methods/mpim.mark) : `Slack::Request::MPIMMark`
- [mpim.open](https://api.slack.com/methods/mpim.open) : `Slack::Request::MPIMOpen`
- [mpim.replies](https://api.slack.com/methods/mpim.replies) : `Slack::Request::MPIMReplies`

### Misc Methods

- [api.test](https://api.slack.com/methods/api.test) : `Slack::Request::APITest`

### Pins Methods

- [pins.add](https://api.slack.com/methods/pins.add) : `Slack::Request::PinsAdd`
- [pins.list](https://api.slack.com/methods/pins.list) : `Slack::Request::PinsList`
- [pins.remove](https://api.slack.com/methods/pins.remove) : `Slack::Request::PinsRemove`

### User Groups Methods

- [usergroups.create](https://api.slack.com/methods/usergroups.create) : `Slack::Request::UserGroupsCreate`
- [usergroups.disable](https://api.slack.com/methods/usergroups.disable) : `Slack::Request::UserGroupsDisable`
- [usergroups.enable](https://api.slack.com/methods/usergroups.enable) : `Slack::Request::UserGroupsEnable`
- [usergroups.list](https://api.slack.com/methods/usergroups.list) : `Slack::Request::UserGroupsList`
- [usergroups.update](https://api.slack.com/methods/usergroups.update) : `Slack::Request::UserGroupsUpdate`
- [usergroups.users.list](https://api.slack.com/methods/usergroups.users.list) : `Slack::Request::UserGroupsUsersList`
- [usergroups.users.update](https://api.slack.com/methods/usergroups.users.update) : `Slack::Request::UserGroupsUsersUpdate`

### User Methods

- [users.conversations](https://api.slack.com/methods/users.conversations) : `Slack::Request::UsersConversations`
- [users.getPresence](https://api.slack.com/methods/users.getPresence) : `Slack::Request::UsersGetPresence`
- [users.info](https://api.slack.com/methods/users.info) : `Slack::Request::UsersInfo`
- [users.list](https://api.slack.com/methods/users.list) : `Slack::Request::UsersList`
- [users.lookupByEmail](https://api.slack.com/methods/users.lookupByEmail) : `Slack::Request::UsersLookupByEmail`
- [users.profile.get](https://api.slack.com/methods/users.profile.get) : `Slack::Request::UsersProfileGet`

## Contributing

1. Fork it (<https://github.com/crystal-bits/slack-api/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [stbaldwin](https://github.com/crystal-bits) Steve Baldwin - creator, maintainer
- [skaragianis](https://github.com/crystal-bits) Steffan Karagianis - maintainer
